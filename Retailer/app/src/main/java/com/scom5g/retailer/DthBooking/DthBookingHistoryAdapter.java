package com.scom5g.retailer.DthBooking;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Scom5GRetailer;

import java.util.List;

public class DthBookingHistoryAdapter extends RecyclerView.Adapter<DthBookingHistoryAdapter.MyViewHolder> {

    private List<Item> billPaymentList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtOperatorName,txtAmount,txtBookingId,txtCustomerName,txtCustomerNo,txtPackage,txtRemark,txtCommission,txtCreatedAt,txtStatus;
        ImageView ImgArrow;
        ConstraintLayout contentMore;
        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txtOperatorName = (TextView) view.findViewById(R.id.txtOperatorName);
            txtAmount = (TextView) view.findViewById(R.id.txtAmount);
            txtBookingId = (TextView) view.findViewById(R.id.txtBookingId);
            txtCustomerName = (TextView) view.findViewById(R.id.txtCustomerName);
            txtCustomerNo = (TextView) view.findViewById(R.id.txtCustomerNo);
            txtPackage = (TextView) view.findViewById(R.id.txtPackage);
            txtRemark = (TextView) view.findViewById(R.id.txtRemark);
            txtCommission = (TextView) view.findViewById(R.id.txtCommission);
            txtCreatedAt = (TextView) view.findViewById(R.id.txtCreatedAt);
            txtStatus = (TextView) view.findViewById(R.id.txtStatus);
            ImgArrow= (ImageView) view.findViewById(R.id.ImgArrow);
            contentMore= (ConstraintLayout) view.findViewById(R.id.contentMore);

            ImgArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility =contentMore.getVisibility();
                    if (visibility == View.VISIBLE){
                        contentMore.setVisibility(View.GONE);
                        ImgArrow.setImageResource(R.drawable.ic_expand_more);
                    }else{
                        contentMore.setVisibility(View.VISIBLE);
                        ImgArrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });
        }
    }

    public DthBookingHistoryAdapter(List<Item> billPaymentList, Context ctx, String frg_name) {
        this.billPaymentList = billPaymentList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public DthBookingHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dth_booking_history_layout, parent, false);

        return new DthBookingHistoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DthBookingHistoryAdapter.MyViewHolder holder, int position) {
        Item movie = billPaymentList.get(position);

        holder.txtOperatorName.setText(movie.getLabel11()+"");
        holder.txtAmount.setText(movie.getLabel12()+"");
        holder.txtBookingId.setText(movie.getLabel13()+"");
        holder.txtCustomerName.setText(movie.getLabel14()+"");
        holder.txtCustomerNo.setText(movie.getLabel15()+"");
        holder.txtPackage.setText(movie.getLabel16()+"");
        holder.txtRemark.setText(movie.getLabel17()+"");
        holder.txtCommission.setText(movie.getLabel18()+"");
        holder.txtCreatedAt.setText(movie.getLabel19()+"");
        holder.txtStatus.setText(movie.getLabel20()+"");

        if (movie.getLabel20().equalsIgnoreCase("Failed")) {
            holder.txtAmount.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_fail));
            holder.txtStatus.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_fail));
        } else if (movie.getLabel20().equalsIgnoreCase("Pending")){
            holder.txtAmount.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_initiated));
            holder.txtStatus.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_initiated));
        }else{
            holder.txtAmount.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_success));
            holder.txtStatus.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_success));
        }
    }

    @Override
    public int getItemCount() {
        return billPaymentList.size();
    }
}
