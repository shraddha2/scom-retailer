package com.scom5g.retailer.Recharge.BrowseRechargePlan;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import android.widget.TextView;

import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.NavigationDrawer.HomeActivity;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Recharge.MobileOperatorFragment;
import com.scom5g.retailer.Scom5GRetailer;

import org.json.JSONException;
import org.json.JSONObject;

public class BrowsePlanActivity extends AppCompatActivity {

    TextView txt_balance, txt_scom;
    String balance = "", token, operator_id;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    boolean loggedin = false;
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;
    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_transaction);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(false);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)
        txt_balance = (TextView) toolbar.findViewById(R.id.txt_balance);
        txt_scom = (TextView) toolbar.findViewById(R.id.txt_scom);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        operator_id=getIntent().getStringExtra("operator_id");
        Log.d("1st",operator_id);
ctx= BrowsePlanActivity.this;
        // Create the adapter that will return a fragment for each of the three primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        loggedin = sharedpreferences.getBoolean(DefineData.KEY_LOGIN_SUCCESS, false);
        token = sharedpreferences.getString(DefineData.TOKEN_KEY, "");

        new FetchBalance().execute();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    FulTTPlanFragment tab1 = new FulTTPlanFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("operator_id", operator_id + "");
                    Log.d("2st",operator_id);
                    tab1.setArguments(bundle);
                    /*Intent i=new Intent(BrowsePlanActivity.this, FulTTPlanFragment.class);
                    i.putExtra(operator_id,operator_id);*/
                    /*android.app.Fragment frg=new MobileOperatorFragment();
                   ((FragmentActivity) BrowsePlanActivity.this).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"FULLTT")
                            .addToBackStack(null)
                            .commit();*/
                    return tab1;
                case 1:
                    TOPUPPlanFragment tab2 = new TOPUPPlanFragment();
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("operator_id", operator_id + "");
                    Log.d("2st",operator_id);
                    tab2.setArguments(bundle2);
                    return tab2;
                case 2:
                    ThreeGFourGPlanFragment tab3 = new ThreeGFourGPlanFragment();
                    Bundle bundle3 = new Bundle();
                    bundle3.putString("operator_id", operator_id + "");
                    Log.d("2st",operator_id);
                    tab3.setArguments(bundle3);
                    return tab3;
                case 3:
                    RATECUTTERPlanFragment tab4 = new RATECUTTERPlanFragment();
                    Bundle bundle4 = new Bundle();
                    bundle4.putString("operator_id", operator_id + "");
                    Log.d("2st",operator_id);
                    tab4.setArguments(bundle4);
                    return tab4;
                case 4:
                    twoGPlanFragment tab5 = new twoGPlanFragment();
                    Bundle bundle5 = new Bundle();
                    bundle5.putString("operator_id", operator_id + "");
                    Log.d("2st",operator_id);
                    tab5.setArguments(bundle5);
                    return tab5;
                case 5:
                    SMSPlanFragment tab6 = new SMSPlanFragment();
                    Bundle bundle6 = new Bundle();
                    bundle6.putString("operator_id", operator_id + "");
                    Log.d("2st",operator_id);
                    tab6.setArguments(bundle6);
                    return tab6;
                case 6:
                    ROMAINGPlanFragment tab7 = new ROMAINGPlanFragment();
                    Bundle bundle7 = new Bundle();
                    bundle7.putString("operator_id", operator_id + "");
                    Log.d("2st",operator_id);
                    tab7.setArguments(bundle7);
                    return tab7;
                case 7:
                    COMBOPlanFragment tab8 = new COMBOPlanFragment();
                    Bundle bundle8 = new Bundle();
                    bundle8.putString("operator_id", operator_id + "");
                    Log.d("2st",operator_id);
                    tab8.setArguments(bundle8);
                    return tab8;
                case 8:
                    FRCPlanFragment tab9 = new FRCPlanFragment();
                    Bundle bundle9 = new Bundle();
                    bundle9.putString("operator_id", operator_id + "");
                    Log.d("2st",operator_id);
                    tab9.setArguments(bundle9);
                    return tab9;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 9;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "FULLTT";
                case 1:
                    return "TOPUP";
                case 2:
                    return "3G/4G";
                case 3:
                    return "RATE CUTTER";
                case 4:
                    return "2G";
                case 5:
                    return "SMS";
                case 6:
                    return "ROMAING";
                case 7:
                    return "COMBO";
                case 8:
                    return "FRC";
            }
            return null;
        }
    }

    private class FetchBalance extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try {

                this.response = new JSONObject(service.POST(DefineData.FETCH_BALANCE, token));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (response != null) {
                //String balance="";
                try {
                    if (response.getBoolean("error")) {
                        balance = "Balance : ₹ 0";
                    } else {
                        balance = response.getString("balance");
                    }
                    txt_balance.setText("₹ " + balance + "");
                    editor.putString(DefineData.KEY_WALLET_BALANCE, balance);
                    editor.commit();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent();
        setResult(5,intent);
        finish();//finishing activity

    }
}
