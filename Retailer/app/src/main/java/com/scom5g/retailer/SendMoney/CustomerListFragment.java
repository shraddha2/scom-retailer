package com.scom5g.retailer.SendMoney;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.FundRequest.FundRequestFragment;
import com.scom5g.retailer.HomePage.HomeFragment;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;
import com.scom5g.retailer.Scom5GRetailer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomerListFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    TextView txt_msgg;
    String token;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_img_bg,rel_no_internet,rel_no_records;
    TextView txt_err_msgg,txt_network_msg;
    EditText edt_search_num;
    ImageView img_search;
    String mobile_number="";
    private String sender_id;
    private String otp_up;
    private List<Item> movieList = new ArrayList<>();
    String errorMsg;
    String mobile_no;

    public CustomerListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_customer_list, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);
        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Scom5GRetailer.getInstance().MODE_PRIVATE);
        editor = sharedpreferences.edit();
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        txt_msgg= (TextView) rootView.findViewById(R.id.txt_msgg);
        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        rel_img_bg= (RelativeLayout) rootView.findViewById(R.id.rel_img_bg);
        rel_no_internet= (RelativeLayout) rootView.findViewById(R.id.rel_no_internet);
        rel_no_records= (RelativeLayout) rootView.findViewById(R.id.rel_no_records);
        edt_search_num=(EditText)rootView.findViewById(R.id.edt_search_number);
        img_search=(ImageView)rootView.findViewById(R.id.img_search);

        linear_container.setVisibility(View.VISIBLE);
        rel_img_bg.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        /*Bundle bundle = getArguments();
        mobile_no = bundle.getString("mobile_no");
        edt_search_num.setText(mobile_no+" ");*/
        mobile_no=sharedpreferences.getString(DefineData.CUSTOMER_MOBILE_NO,"");
        edt_search_num.setText(mobile_no+"");

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(getActivity());
                String search_txt=edt_search_num.getText().toString();
                mobile_number=search_txt;
                boolean isError=false;
                if(null==mobile_number||mobile_number.length()<10||mobile_number.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_search_num.setError("Invalid Mobile Number");
                }
                if (!isError) {
                    if (checkConnection()) {
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                        new FetchCustomers().execute(search_txt);

                    } else {
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                        rel_no_records.setVisibility(View.GONE);
                    }
                }
            }
        });
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2)
        {
            String msgg="";
            if(null!=data)
            {
                msgg=data.getStringExtra("MESSAGE");
            }

            linear_container.setVisibility(View.VISIBLE);
            rel_img_bg.setVisibility(View.VISIBLE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);

            if(msgg.equalsIgnoreCase("success")) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Success");
                builder.setMessage("Customer Added Successfully.Kindly Add Beneficiary");
                builder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                hideKeyboard(getActivity());
                                String search_txt = edt_search_num.getText().toString();
                                mobile_number = search_txt;
                                boolean isError = false;
                                if(null==mobile_number||mobile_number.length()<10||mobile_number.equalsIgnoreCase(""))
                                {
                                    isError=true;
                                    edt_search_num.setError("Invalid Mobile Number");
                                }
                                if (!isError) {
                                    if (checkConnection()) {
                                        linear_container.setVisibility(View.VISIBLE);
                                        progress_linear.setVisibility(View.GONE);
                                        rel_no_internet.setVisibility(View.GONE);
                                        rel_no_records.setVisibility(View.GONE);
                                        new FetchCustomers().execute(search_txt);

                                    } else {
                                        linear_container.setVisibility(View.GONE);
                                        progress_linear.setVisibility(View.GONE);
                                        rel_no_internet.setVisibility(View.VISIBLE);
                                        rel_no_records.setVisibility(View.GONE);
                                    }
                                }
                            }
                        });
                builder.show();
            }
        }
    }

    private class FetchCustomers extends AsyncTask<String, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(String... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("senderMobileNo", params[0]);
                this.response = new JSONObject(service.POST(DefineData.SEARCH_CUSTOMER,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("responsefghd" , response + " " +DefineData.SEARCH_CUSTOMER);
            if(response!=null){
                try {
                    boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                    }
                    //Add Customer
                    if (responseCode==22) {
                        linear_container.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.VISIBLE);
                        rel_no_records.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);

                    /*    Intent i=new Intent(getActivity(), AddCustomerActivity.class);
                        i.putExtra("mobile_number",mobile_number);
                        startActivityForResult(i, 2);*/
                        //getActivity().finish();
                        Bundle bundle = new Bundle();
                        bundle.putString("mobile_number", mobile_number);
                        Fragment frg = new AddCustomerActivity();
                        frg.setArguments(bundle);
                        getActivity().getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg, "Add Customer")
                                .addToBackStack(null)
                                .commit();

                    }else if (responseCode==23) { // verify the customer if not

                        JSONObject json=response.getJSONObject("data");
                        sender_id = json.getString("sender_id");

                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Verify Customer Mobile Number");
                        builder.setMessage("OTP has send on registered mobile number");
                        builder.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                        new SendOtp().execute();
                                        showChangeLangDialog(false,"");
                                    }
                                });
                        builder.setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                        Fragment frg = new CustomerListFragment();
                                        getActivity().getFragmentManager().beginTransaction()
                                                .replace(R.id.frg_replace, frg, "Add Customer")
                                                .addToBackStack(null)
                                                .commit();
                                    }
                                });
                        builder.show();
                    }
                    else if(responseCode==1) { //Show beneficiary list

                        JSONObject json = response.getJSONObject("data");
                        JSONArray jsonArray = json.getJSONArray("beneficiaries");
                        Bundle bundle = new Bundle();
                        bundle.putString("json_remitter", json + "");
                        bundle.putString("jsonArray", jsonArray + "");
                        Fragment frg = new BeneficiaryListsFragment();
                        frg.setArguments(bundle);
                        getActivity().getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg, "Beneficiary Lists")
                                .addToBackStack(null)
                                .commit();
                        linear_container.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.VISIBLE);
                        rel_no_records.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    }else if(responseCode==151) {

                        JSONObject json = response.getJSONObject("data");
                        JSONArray jsonArray = json.getJSONArray("beneficiaries");
                        Bundle bundle = new Bundle();
                        bundle.putString("json_remitter", json + "");
                        bundle.putString("jsonArray", jsonArray + "");
                        Fragment frg = new BeneficiaryListsFragment();
                        frg.setArguments(bundle);
                        getActivity().getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg, "Beneficiary Lists")
                                .addToBackStack(null)
                                .commit();

                        linear_container.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.VISIBLE);
                        rel_no_records.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    }else{
                        txt_err_msgg.setText(msg);
                        linear_container.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Something went wrong");
                    linear_container.setVisibility(View.GONE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                }
            }else{
                txt_err_msgg.setText("No Internet Connection");
                linear_container.setVisibility(View.GONE);
                rel_img_bg.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
            }
        }
    }

    private void showChangeLangDialog(boolean isError, String msg) {
        final View dialogView = View.inflate(getActivity(),R.layout.custom_otp_dialog,null);

        final Dialog dialog = new Dialog(getActivity(),R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        final EditText edt_otp=(EditText)dialog.findViewById(R.id.input_otp);
        final TextView txt_Error=(TextView) dialog.findViewById(R.id.serverError);
        Button btn_verify = (Button) dialog.findViewById(R.id.btn_verify);
        Button btn_resend = (Button) dialog.findViewById(R.id.btn_resend);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        if(isError)
        {
            //txt_Error.setError(errorMsg+"");
            txt_Error.setText(errorMsg+"");
        }

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp_up=edt_otp.getText().toString();
                if (checkConnection()) {
                    boolean isError=false;

                    if(null==otp_up||otp_up.length()==0||otp_up.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_otp.setError("Field Cannot be Blank");
                    }
                    if(!isError)
                    {
                        new Verifyotp().execute();
                        dialog.dismiss();
                    }

                }else
                {
                   /* txt_Error.setVisibility(View.VISIBLE);
                    txt_Error.setText("Internet Not Available");*/
                }
            }
        });

        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new VerifyCustomer().execute();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //onBackPressed();
                Fragment frg = new CustomerListFragment();
                getActivity().getFragmentManager().beginTransaction()
                        .replace(R.id.frg_replace, frg, "Add Customer")
                        .addToBackStack(null)
                        .commit();
            }
        });

        dialog.show();
    }

    private class Verifyotp extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
            rel_no_records.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("sender_id",sender_id);
                parameters.put("otp",otp_up);
                this.response = new JSONObject(service.POST(DefineData.VERIFY_OTP,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                        errorMsg = msg;
                        //Log.d("fourMsg",errorMsg+"");
                    }
                    Log.d("sdrsfte","rdtdtdg");
                    if (responseCode==1)
                    {
                        Log.d("sdhcbhj","dcgbsdcbsuydc");
                        JSONObject json=response.getJSONObject("data");
                        mobile_number = json.getString("senderMobileNo");
                        editor.putString(DefineData.CUSTOMER_MOBILE_NO, mobile_number);
                        editor.commit();
                        edt_search_num.setText(mobile_number+"");
                        linear_container.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.VISIBLE);
                        rel_no_records.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    }
                    else{
                        showChangeLangDialog(true,msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class VerifyCustomer extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
            rel_no_records.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("mobileNo",sender_id);
                this.response = new JSONObject(service.POST(DefineData.RESEND_OTP,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                    }
                    if (responseCode==1)
                    {
                        linear_container.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);

                        showChangeLangDialog(false,"");
                    }
                    else{
                        edt_search_num.setText("Error Code : "+responseCode);
                        linear_container.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    linear_container.setVisibility(View.VISIBLE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.GONE);
                }
            }else{
                linear_container.setVisibility(View.VISIBLE);
                rel_img_bg.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.GONE);
            }
        }
    }

    private class SendOtp extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
            rel_no_records.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("mobileNo",sender_id);
                this.response = new JSONObject(service.POST(DefineData.RESEND_OTP,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                    }
                    if (responseCode==1)
                    {
                        linear_container.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                    }
                    else{
                        edt_search_num.setText("Error Code : "+responseCode);
                        linear_container.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    linear_container.setVisibility(View.VISIBLE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.GONE);
                }
            }else{
                linear_container.setVisibility(View.VISIBLE);
                rel_img_bg.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onResume() {

        super.onResume();
        Scom5GRetailer.getInstance().setConnectivityListener(this);

        edt_search_num.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_search_num.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    //Fragment frg=new MoneyTransferFragment();
                    Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Money Transfer")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }
                return false;
            }
        });
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
