package com.scom5g.retailer.BillPayment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.scom5g.retailer.R;

import java.util.ArrayList;

public class BBPSOperatotListActivity extends AppCompatActivity {

    private ArrayList<BillData> movieList = new ArrayList<>();
    private BBPSOperatorListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbpsoperatot_list);

        //setTitle("Select Operator");
        int type = getIntent().getIntExtra("type", 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView txt_error = findViewById(R.id.txt_error);
        RecyclerView recyclerView = findViewById(R.id.rc_retailers);
        movieList.clear();
        movieList = fill_with_data1();

        mAdapter = new BBPSOperatorListAdapter(movieList,this, type);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }

    private ArrayList<BillData> fill_with_data1() {

        ArrayList<BillData> data = new ArrayList<>();
        data.add(new BillData( R.drawable.tata_power, "Tata Power - Mumbai","30"));
        data.add(new BillData( R.drawable.ic_sndl, "SNDL Power - Nagpur","33"));
        data.add(new BillData( R.drawable.ic_best, "Best Undertaking - Mumbai","31"));
        data.add(new BillData( R.drawable.ic_torent, "Torrent Power  - Mumbai","28"));
        data.add(new BillData( R.drawable.ic_msedc, "MSEDC - Maharashtra","27"));
        data.add(new BillData( R.drawable.adani_power, "Adani Power","32"));
        data.add(new BillData( R.drawable.ic_wwe, "WBSEDCL - WEST BENGAL","42"));

        return data;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        Drawable drawable = menu.findItem(R.id.search).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(getApplicationContext(),android.R.color.white));
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) search.getActionView();
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(BBPSOperatotListActivity.this,BillActivity.class);
        startActivity(i);
        finish();
    }
}
