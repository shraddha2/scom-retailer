package com.scom5g.retailer.SendMoney;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.NavigationDrawer.HomeActivity;
import com.scom5g.retailer.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by shraddha on 09-07-2018.
 */

public class BeneficiaryListAdapter extends RecyclerView.Adapter<BeneficiaryListAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String cust_id="", sender_no="", senderName="",neftLimitRs="", cust_mobile="";
    SharedPreferences sharedpreferences;
    String token;
    String beneficiary_id="";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_label5,txt_label6,txt_label7;
        ImageView img_delete,img_send;

        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txt_label6 = (TextView) view.findViewById(R.id.txt_label6);
            txt_label7= (TextView)  view.findViewById(R.id.txt_label7);
            img_delete= (ImageView) view.findViewById(R.id.img_delete);
            img_send= (ImageView) view.findViewById(R.id.img_send);

            img_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    beneficiary_id=txt_label6.getText().toString();

                    final AlertDialog.Builder builder = new AlertDialog.Builder(
                            ctx);
                    builder.setTitle("Confirmation");
                    builder.setMessage("Are you sure you want to delete beneficiary?");
                    builder.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    new DeleteBeneficiary().execute();

                                }
                            });
                    builder.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.dismiss();
                                }
                            });
                    builder.show();
                }
            });

            img_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    beneficiary_id=txt_label6.getText().toString();
                    String account_no=txt_label3.getText().toString();
                    String cust_name=txt_label1.getText().toString();
                    String bank_name=txt_label2.getText().toString();
                    account_no = account_no.replaceAll("A/C No :\n","");
                    Intent i=new Intent(ctx, SendMoneyActivity.class);
                    Log.d("sendMoneyhhh",   cust_id + " " + sender_no+" "+senderName+" " + beneficiary_id + " " + account_no +" " +
                            cust_name + " "+ bank_name + " "+ neftLimitRs);
                    i.putExtra("cust_id",cust_id);
                    i.putExtra("sender_no",sender_no);
                    i.putExtra("senderName",senderName);
                    i.putExtra("beneficiaryId",beneficiary_id);
                    i.putExtra("accountNo",account_no);
                    i.putExtra("beneficiaryName",cust_name);
                    i.putExtra("bankName",bank_name);
                    i.putExtra("neftLimitRs",neftLimitRs);
                    ((Activity)ctx).startActivity(i);
                }
            });
        }
    }

    public BeneficiaryListAdapter(List<Item> moviesList, Context ctx, String cust_id,String sender_no, String senderName, String neftLimitRs) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.cust_id = cust_id;
        this.sender_no = sender_no;
        this.senderName = senderName;
        this.neftLimitRs = neftLimitRs;
        sharedpreferences = ctx.getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.beneficiary_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);
        holder.txt_label1.setText(movie.getTrans_no()+"");
        holder.txt_label3.setText("A/C No :\n"+movie.getTrans_amount()+"");
        holder.txt_label2.setText("Bank Name :\n"+movie.getTrans_type()+"");
        holder.txt_label4.setText("Limit Reached : "+movie.getTrans_update_balance()+"");
        holder.txt_label5.setText("Limit Left : "+movie.getTrans_datetime()+"");
        holder.txt_label6.setText(movie.getStatus());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


    private class DeleteBeneficiary extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("beneficiaryId", beneficiary_id);
                parameters.put("senderId", sender_no);
                //parameters.put("senderId", cust_id);
                this.response = new JSONObject(service.POST(DefineData.DELETE_BENEFICIARY,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("dfrg", response + " "+beneficiary_id+" "+sender_no+" "+DefineData.DELETE_BENEFICIARY);
            String msg="";
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        msg=response.getString("responseDesc");
                    } else {
                        msg=response.getString("responseDesc");
                    }
                    Toast.makeText(ctx, "Deleted Successfully", Toast.LENGTH_LONG).show();
                    Intent i=new Intent(ctx, HomeActivity.class);
                    ctx.startActivity(i);
                    ((Activity)ctx).finish();

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }else{

            }

        }
    }
}
