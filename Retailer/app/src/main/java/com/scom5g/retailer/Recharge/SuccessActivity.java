package com.scom5g.retailer.Recharge;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.scom5g.retailer.NavigationDrawer.HomeActivity;
import com.scom5g.retailer.OfflineMode.OfflineModeActivity;
import com.scom5g.retailer.R;

public class SuccessActivity extends AppCompatActivity {

    Button btn_home;
    TextView txt_msg,txt_type;
    String type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String mess = getResources().getString(R.string.app_name);
        setTitle(mess);

        Button btn_home=(Button)  findViewById(R.id.btn_home);
        txt_msg=(TextView)  findViewById(R.id.txt_msg);
        txt_type=(TextView)  findViewById(R.id.txt_type);

        String msg=getIntent().getStringExtra("msg");
        type=getIntent().getStringExtra("type");
        txt_msg.setText(msg);
        txt_type.setText(type);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i;
        if(type.equalsIgnoreCase("Offline Recharge"))
        {
            i = new Intent(SuccessActivity.this, OfflineModeActivity.class);
        }else {
            i = new Intent(SuccessActivity.this, HomeActivity.class);
        }
        startActivity(i);
        finish();

    }
}
