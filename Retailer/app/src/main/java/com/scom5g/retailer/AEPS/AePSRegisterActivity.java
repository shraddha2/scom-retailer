package com.scom5g.retailer.AEPS;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.minkspay.minkspayaepslibrary.MinkspayRegisterActivity;
import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.NavigationDrawer.HomeActivity;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;
import com.scom5g.retailer.Scom5GRetailer;

import org.json.JSONException;
import org.json.JSONObject;

public class AePSRegisterActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener  {

    private static final int REQUEST_CODE = 100;
    ConstraintLayout loading,aepsContent,aepsBag;
    TextView txtError,txtMessage,txtStatus,txtTransactionType,txtBankRefNo,txtBalAmt,txtTransAmt;
    Button btnBack,btnRepeatTransaction;
    SharedPreferences sharedpreferences;
    String token="",Latitude="",Longitude="", IMEINumber="", sdkstatus="";
    ImageView imgStatus;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0;
    // GPSTracker class
    GPSTracker gps;
    Context mContext;

    //imei
    TelephonyManager tel;

    public AePSRegisterActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aeps_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("AEPS");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Scom5GRetailer.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        loading = (ConstraintLayout) findViewById(R.id.loading);
        txtError = (TextView) findViewById(R.id.txtError);
       /* rel_no_internet= (ConstraintLayout) findViewById(R.id.rel_no_internet);
        rel_no_records= (ConstraintLayout) findViewById(R.id.rel_no_records);*/

        aepsContent = (ConstraintLayout) findViewById(R.id.aepsContent);
        aepsBag = (ConstraintLayout) findViewById(R.id.aepsBag);
        imgStatus = (ImageView) findViewById(R.id.imgStatus);
        txtMessage = (TextView) findViewById(R.id.txtMessage);
        txtStatus = (TextView) findViewById(R.id.txtStatus);
        txtTransactionType = (TextView) findViewById(R.id.txtTransactionType);
        txtBankRefNo = (TextView) findViewById(R.id.txtBankRefNo);
        txtBalAmt = (TextView) findViewById(R.id.txtBalAmt);
        txtTransAmt = (TextView) findViewById(R.id.txtTransAmt);

        btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(AePSRegisterActivity.this, HomeActivity.class);
                startActivity(i);
            }
        });

        btnRepeatTransaction = (Button) findViewById(R.id.btnRepeatTransaction);
        btnRepeatTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetAepsKeyDetails().execute();
            }
        });

        //Get Lati, Long
        mContext = this;
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED

                && ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AePSRegisterActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            Toast.makeText(mContext, "You need have granted permission", Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(mContext, AePSRegisterActivity.this);

            // Check if GPS enabled

            if (gps.canGetLocation()) {

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

                // \n is for new line

                /*Toast.makeText(getApplicationContext(),
                        "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();*/
                //Log.d("cvcc",latitude+" "+longitude+" ");
                Latitude=Double.toString(latitude);
                Longitude=Double.toString(longitude);;
                // Log.d("tytr",Latitude+" "+Longitude+" ");

            } else {
                gps.showSettingsAlert();
            }
        }

        //Get imei
        loadIMEI();
        /** Fading Transition Effect */
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        new GetAepsKeyDetails().execute();

    }

    //Get the key details
    private class GetAepsKeyDetails extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.GONE);
            //rel_no_records.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.GET_AEPS_KEY,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("getAepsKeyResponse", response+ " dgfcxgs"+""+" "+DefineData.GET_AEPS_KEY+" ");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        //Log.d("dchxgcvh",msg+" ");
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(msg+"");
                        //rel_no_records.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                    } else {
                        JSONObject json = response.getJSONObject("data");
                        if(json.length()!=0) {

                            try {

                                String developer_key = json.getString("developer_key");
                                String developer_pass = json.getString("developer_pass");
                                String retailer_mobile_number = json.getString("retailer_mobile_number");
                                String partner_agent_id = json.getString("partner_agent_id");
                                String txn_req_id = json.getString("txn_req_id");

                                Log.d("AepsResponse","developer_key: "+developer_key+"\n"+"developer_pass: "+developer_pass+"\n"+"retailer_mobile_number: "+retailer_mobile_number+"\n"+"partner_agent_id: "+partner_agent_id+"\n"+"txn_req_id: "+txn_req_id+" ");

                                Intent i=new Intent(AePSRegisterActivity.this, MinkspayRegisterActivity.class);
                                Log.d("dfc","sdcsdc");
                                i.putExtra("developer_key",developer_key);
                                i.putExtra("developer_pass",developer_pass);
                                i.putExtra("retailer_mobile_number",retailer_mobile_number);
                                i.putExtra("partner_agent_id",partner_agent_id);
                                i.putExtra("txn_req_id",txn_req_id);
                                i.putExtra("actionbar_title","AEPS");
                                i.putExtra("latitude",Latitude);
                                i.putExtra("​longitude​", Longitude);
                                i.putExtra("​imei​",IMEINumber);
                                Log.d("opopopop","developer_key: "+developer_key+"\n"+"developer_pass: "+developer_pass+"\n"+"retailer_mobile_number: "+
                                        retailer_mobile_number+"\n"+"partner_agent_id: "+partner_agent_id+"\n"+"txn_req_id: "+txn_req_id+" "+
                                        "Latitude: "+Latitude+"\n"+"Longitude: "+Longitude+"\n"+"Imei: "+IMEINumber+"");

                                startActivityForResult(i,REQUEST_CODE);

                                //rel_no_records.setVisibility(View.GONE);
                                loading.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                txtError.setVisibility(View.VISIBLE);
                                txtError.setText("Error in parsing response");
                                //rel_no_records.setVisibility(View.VISIBLE);
                                loading.setVisibility(View.GONE);
                            }
                        }else{
                            txtError.setVisibility(View.VISIBLE);
                            txtError.setText("Record Not Available");
                            //rel_no_records.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("Error in parsing response");
                    //rel_no_records.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);
                }
            }else{
                txtError.setVisibility(View.VISIBLE);
                txtError.setText("Empty Server Response");
                //rel_no_records.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
            }
        }
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data)  {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE && resultCode ==RESULT_OK)
        {
            String message = data.getStringExtra("message");
            boolean status = data.getBooleanExtra("status",false);
            String transType = data.getStringExtra("transType");
            String bankRrn = data.getStringExtra("bankRrn");
            Double balAmount = data.getDoubleExtra("balAmount",0.0);
            Double transAmount = data.getDoubleExtra("transAmount",0.0);

            Log.d("dcbnzn","vfhb");
            Log.d("SdkResponse","Message: "+message+" "+"Status: "+status+" "+"TransType: "+transType+" "+"BankRrn: "+bankRrn+" "+"BalAmount: "+balAmount+" "+"TransAmount: "+transAmount+" ");
            aepsBag.setVisibility(View.GONE);
            aepsContent.setVisibility(View.VISIBLE);
            txtMessage.setText(": "+message+"");
            txtStatus.setText(": "+status+"");
            txtTransactionType.setText(": "+transType+"");
            txtBankRefNo.setText(": "+bankRrn+"");
            txtBalAmt.setText(": "+balAmount+"");
            txtTransAmt.setText(": "+transAmount+"");

            if(status==true){
                imgStatus.setImageResource(R.drawable.ic_success);
            }
            else
            {
                imgStatus.setImageResource(R.drawable.ic_fail);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent =new Intent(this, HomeActivity.class);
                startActivity(intent);
                finish();
                //return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void  onBackPressed() {
        super.onBackPressed();
        Intent intent =new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }


    //Get IMEI number
    public void loadIMEI() {
        // Check if the READ_PHONE_STATE permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // READ_PHONE_STATE permission has not been granted.
            requestReadPhoneStatePermission();
        } else {
            // READ_PHONE_STATE permission is already been granted.
            doPermissionGrantedStuffs();
        }
    }

    private void requestReadPhoneStatePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_PHONE_STATE)) {

            new AlertDialog.Builder(AePSRegisterActivity.this)
                    .setTitle("Permission Request")
                    .setMessage(getString(R.string.permission_read_phone_state_rationale))
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //re-request
                            ActivityCompat.requestPermissions(AePSRegisterActivity.this,
                                    new String[]{Manifest.permission.READ_PHONE_STATE},
                                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
                        }
                    })
                    .setIcon(R.drawable.ic_fail)
                    .show();
        } else {
            // READ_PHONE_STATE permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == MY_PERMISSIONS_REQUEST_READ_PHONE_STATE) {
            // Received permission result for READ_PHONE_STATE permission.est.");
            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // READ_PHONE_STATE permission has been granted, proceed with displaying IMEI Number
                //alertAlert(getString(R.string.permision_available_read_phone_state));
                doPermissionGrantedStuffs();
            } else {
                alertAlert(getString(R.string.permissions_not_granted_read_phone_state));
            }
        }
    }

    private void alertAlert(String msg) {
        new AlertDialog.Builder(AePSRegisterActivity.this)
                .setTitle("Permission Request")
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(R.drawable.ic_fail)
                .show();
    }


    public void doPermissionGrantedStuffs() {

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        IMEINumber = tm.getDeviceId();

    }
}