package com.scom5g.retailer.Recharge;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scom5g.retailer.FundRequest.FundBankDetails;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;

import java.util.List;

public class SpecialPlanAdapter extends RecyclerView.Adapter<SpecialPlanAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2;

        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String amount=txt_label2.getText().toString();
                    Intent intent=new Intent(ctx, SpecialPlanActivity.class);
                    intent.putExtra("operator_circle_name",amount);
                    ((Activity) ctx).setResult(22,intent);
                    ((Activity) ctx).finish();//finishing activity
                }
            });
        }
    }


    public SpecialPlanAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.special_plan_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_label1.setText(movie.getOperator_name()+"");
        holder.txt_label2.setText(movie.getCommission()+"");
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
