package com.scom5g.retailer.BillPayment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.scom5g.retailer.R;

import java.util.ArrayList;

public class BBPSOperatorListAdapter extends RecyclerView.Adapter<BBPSOperatorListAdapter.ViewHolder> implements Filterable {
    private final ArrayList<BillData> mArrayList;
    private ArrayList<BillData> mFilteredList;
    private final Context ctx;
    private final int rech_type;

    public BBPSOperatorListAdapter(ArrayList<BillData> arrayList, Context ctx, int rech_type) {
        mArrayList = arrayList;
        mFilteredList = arrayList;
        this.ctx=ctx;
        this.rech_type=rech_type;
    }

    @Override
    public BBPSOperatorListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.operator_list_layout, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BBPSOperatorListAdapter.ViewHolder viewHolder, int i) {
        viewHolder.txt_operator_name.setText(mFilteredList.get(i).getTxt());
        viewHolder.txt_id.setText(mFilteredList.get(i).getTxt_id());
        viewHolder.img_operator_logo.setImageResource(mFilteredList.get(i).getImageId());
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<BillData> filteredList = new ArrayList<>();

                    for (BillData androidVersion : mArrayList) {

                        if (androidVersion.getTxt().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                //noinspection unchecked
                mFilteredList = (ArrayList<BillData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView txt_operator_name;
        private final TextView txt_id;
        private final ImageView img_operator_logo;
        public ViewHolder(View view) {
            super(view);

            txt_operator_name = view.findViewById(R.id.txtOperatorName);
            txt_id = view.findViewById(R.id.txtOperatorId);
            img_operator_logo = view.findViewById(R.id.imgOperator);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String operator_name=txt_operator_name.getText().toString();
                    String id=txt_id.getText().toString();
                    Intent intent=new Intent(ctx, NonBBPSOperatorListActivity.class);
                    intent.putExtra("operator_name",operator_name);
                    intent.putExtra("operator_id",id);
                    ((Activity) ctx).setResult(5,intent);
                    ((Activity) ctx).finish();//finishing activity

                }
            });

        }
    }
}
