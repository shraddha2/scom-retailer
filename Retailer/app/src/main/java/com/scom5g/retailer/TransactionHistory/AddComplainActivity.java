package com.scom5g.retailer.TransactionHistory;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.LastTenTransaction.CustomSpinnerAdapter;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.NavigationDrawer.HomeActivity;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;
import com.scom5g.retailer.Scom5GRetailer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddComplainActivity extends Activity implements ConnectivityReceiver.ConnectivityReceiverListener {
    TextView txt_title,txt_label1,txt_label2,txt_label3,txt_label4;
    EditText edt_desc,edt_amt,edt_recharge_id;
    Button btn_submit;
    Spinner spin_com_type;
    List<Item> list_comp_type;
    String trans_id;
    CustomSpinnerAdapter sp_adapter = null;
    String comp_type="--Select--",comp_type_id="0",complain_descrip="";
    String token="";
    SharedPreferences sharedpreferences;
    TextView txt_error;
    LinearLayout progress_linear,linear_container;

    public AddComplainActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_complaint);

        trans_id=getIntent().getStringExtra("trans_id");
        Log.d("sdbcgsh",trans_id+" ");
        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        list_comp_type=new ArrayList<>();
        txt_title= (TextView) findViewById(R.id.txt_title);
        txt_label1= (TextView) findViewById(R.id.txt_label1);
        txt_label2= (TextView) findViewById(R.id.txt_label2);
        txt_label3= (TextView) findViewById(R.id.txt_label3);
        txt_label4= (TextView) findViewById(R.id.txt_label4);
        btn_submit= (Button) findViewById(R.id.btn_submit);
        edt_desc= (EditText) findViewById(R.id.edt_desc);
        edt_amt= (EditText) findViewById(R.id.edt_amt);
        spin_com_type= (Spinner) findViewById(R.id.spin_com_type);
        edt_recharge_id= (EditText) findViewById(R.id.edt_recharge_id);
        txt_error= (TextView) findViewById(R.id.txt_error);
        progress_linear= (LinearLayout) findViewById(R.id.loding);
        linear_container= (LinearLayout) findViewById(R.id.container);

        progress_linear.setVisibility(View.GONE);
        linear_container.setVisibility(View.VISIBLE);



        loadComplainttype();
        spin_com_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                comp_type = ((TextView)view.findViewById(R.id.txt_title)).getText().toString();
                comp_type_id = ((TextView)view.findViewById(R.id.txt_id)).getText().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                complain_descrip=edt_desc.getText().toString();
                if (checkConnection()) {
                    boolean isError=false;
                    if(null==complain_descrip||complain_descrip.length()==0||complain_descrip.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_desc.setError("Field Cannot be Blank");
                    }
                    if(null==comp_type_id||comp_type_id.length()==0||comp_type_id.equalsIgnoreCase("0"))
                    {
                        isError=true;
                        ((TextView)spin_com_type.findViewById(R.id.txt_title)).setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),android.R.color.holo_red_dark));
                    }
                    if(!isError)
                    {
                        new SendCompplaint().execute();
                    }
                }else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("No Internet Connection");
                }
            }
        });
    }

    private  void loadComplainttype()
    {
        list_comp_type.clear();
        list_comp_type.add(0,new Item("--Select--","0"));
        list_comp_type.add(new Item("Recharge Pending for Long Time","1"));
        list_comp_type.add(new Item("Recharge Done but Balance Not Received","2"));
        list_comp_type.add(new Item("Recharge Reversal","3"));
        list_comp_type.add(new Item("Other","4"));
        sp_adapter =new CustomSpinnerAdapter(R.layout.spinner_item, list_comp_type);
        sp_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_com_type.setAdapter(sp_adapter);
    }

    private class SendCompplaint extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("recharge_id", trans_id);
                parameters.put("complaint_type", comp_type_id);
                parameters.put("complaint_desc", complain_descrip);
                this.response = new JSONObject(service.POST(DefineData.SEND_COMPLAINT,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("message");
                        txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText(message+"");
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        //txt_error.setTextColor(ContextCompat.getColor(getActivity(),R.color.status_fail));
                    } else {
                        String msg=response.getString("data");

                        Toast.makeText(AddComplainActivity.this,msg+"", Toast.LENGTH_LONG).show();
                        Intent i=new Intent(AddComplainActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                txt_error.setVisibility(View.VISIBLE);
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onResume() {

        super.onResume();
        Scom5GRetailer.getInstance().setConnectivityListener(this);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
