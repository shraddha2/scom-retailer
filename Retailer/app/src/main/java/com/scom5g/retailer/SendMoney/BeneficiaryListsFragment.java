package com.scom5g.retailer.SendMoney;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.DividerItemDecoration;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;
import com.scom5g.retailer.Scom5GRetailer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BeneficiaryListsFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private BeneficiaryListAdapter mAdapter;
    String search_num,token;
    SharedPreferences sharedpreferences;
    String cust_id="", sender_no="", senderName="",cust_name="";
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_no_records,rel_no_internet;
    Button btn_add_benef;
    String cust_mobile,remaininglimit;
    SwipeRefreshLayout mSwipeRefreshLayout;
    Button btn_refresh;
    String status;

    // Typeface font ;
    public BeneficiaryListsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_beneficiary_lists, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);
        setHasOptionsMenu(true);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Scom5GRetailer.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_recharge_history);
        //txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        btn_add_benef= (Button) rootView.findViewById(R.id.btn_add_benef);
        btn_refresh= (Button) rootView.findViewById(R.id.btn_refresh);

        TextView txt_cust_name= (TextView) rootView.findViewById(R.id.txt_cust_name);
        TextView txt_mb_no= (TextView) rootView.findViewById(R.id.txt_mb_no);
        TextView txt_limit= (TextView) rootView.findViewById(R.id.txt_limit);
        TextView txt_used_limit= (TextView) rootView.findViewById(R.id.txt_used_limit);

        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        rel_no_records= (RelativeLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (RelativeLayout) rootView.findViewById(R.id.rel_no_internet);

        Bundle bundle = getArguments();
        String jsonobj = bundle.getString("json_remitter");
        String jsonArray_b = bundle.getString("jsonArray");

        try {

            JSONObject json_remitter = new JSONObject(jsonobj);
            cust_id = json_remitter.getString("senderId");
            cust_name = json_remitter.getString("senderName");
            cust_mobile = json_remitter.getString("senderMobileNo");
            String consumedlimit = json_remitter.getString("consumedLimitRs");
            remaininglimit = json_remitter.getString("neftLimitRs");
            txt_cust_name.setText("Name : " + cust_name);
            txt_mb_no.setText("Number : " + cust_mobile);
            txt_limit.setText("Limit : " + remaininglimit);
            txt_used_limit.setText("Used Limit : " + consumedlimit);

            JSONArray json_benef_array = new JSONArray(jsonArray_b);
            if (json_benef_array.length() != 0) {
                movieList.clear();
                for (int i = 0; i < json_benef_array.length(); i++) {
                    Item superHero = null;
                    JSONObject json2 = null;
                    try {
                        //Getting json
                        json2 = json_benef_array.getJSONObject(i);
                        String beneficiary_id = json2.getString("beneficiary_id");
                        String beneficiary_name = json2.getString("beneficiary_name");
                        String account_no = json2.getString("beneficiary_account_no");
                        String bank_name = json2.getString("beneficiary_bank_name");

                        superHero = new Item(beneficiary_name, bank_name, account_no, "1", "", beneficiary_id);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        txt_err_msgg.setText("Something went wrong");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    }
                    //Adding the superhero object to the list
                    movieList.add(superHero);
                }
                mAdapter = new BeneficiaryListAdapter(movieList,getActivity(),cust_id,cust_mobile,remaininglimit, remaininglimit);
                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
                linear_container.setVisibility(View.VISIBLE);
                rel_no_records.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }else{
                txt_err_msgg.setText("Sorry ! Beneficiary Lists Not Available");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        btn_add_benef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(Scom5GRetailer.getInstance(), AddBeneficiaryActivity.class);
                i.putExtra("cust_id",cust_id+"");
                i.putExtra("mobile_no",cust_mobile+"");
                i.putExtra("limit",remaininglimit+"");
                i.putExtra("senderName",cust_name+"");
                startActivity(i);
            }
        });

        return rootView;
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private void hideKeyboard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
        Scom5GRetailer.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    Fragment frg=new CustomerListFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"customer lists")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }


}
