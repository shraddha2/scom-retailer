package com.scom5g.retailer.BusBooking;

/**
 * Created by sai116 on 11/16/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Scom5GRetailer;
import com.scom5g.retailer.TransactionHistory.AddComplainActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BusBookingHistoryAdapter extends RecyclerView.Adapter<BusBookingHistoryAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_transaction_id, txt_destination, bus_name,txt_status,bus_type,txt_name,txt_balance,txt_date;
        ImageView img_arrow;
        LinearLayout linear_more_data;
        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txt_transaction_id = (TextView) view.findViewById(R.id.txt_transaction_id);
            txt_destination = (TextView) view.findViewById(R.id.txt_destination);
            bus_name = (TextView) view.findViewById(R.id.bus_name);
            txt_status = (TextView) view.findViewById(R.id.txt_status);
            bus_type = (TextView) view.findViewById(R.id.bus_type);
            txt_name = (TextView) view.findViewById(R.id.txt_name);
            txt_balance = (TextView) view.findViewById(R.id.txt_balance);
            txt_date = (TextView) view.findViewById(R.id.txt_date);
            img_arrow= (ImageView) view.findViewById(R.id.img_arrow);
            linear_more_data= (LinearLayout) view.findViewById(R.id.linear_more_data);

            img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility =linear_more_data.getVisibility();
                    if (visibility == View.VISIBLE){
                        linear_more_data.setVisibility(View.GONE);
                        img_arrow.setImageResource(R.drawable.ic_expand_more);
                    }else{

                        linear_more_data.setVisibility(View.VISIBLE);
                        img_arrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });

        }
    }


    public BusBookingHistoryAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bus_booking_history_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_transaction_id.setText(movie.getLabel11()+"");
        holder.txt_destination.setText(movie.getLabel12()+"");
        holder.bus_name.setText("Trans. No.: "+movie.getLabel13());
        holder.txt_status.setText("Commission.: "+movie.getLabel14()+"");
        holder.bus_type.setText(movie.getLabel15()+"");
        holder.txt_name.setText("Operator : "+movie.getLabel16()+"");
        holder.txt_balance.setText(DefineData.parseDateToddMMyyyyhh(movie.getLabel17())+"");
        holder.txt_date.setText(movie.getLabel18()+"");


       /* if (movie.getLabel18().equalsIgnoreCase("Failed")) {
            holder.txt_label2.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_fail));
            holder.txt_label8.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_fail));
        } else if (movie.getLabel18().equalsIgnoreCase("Pending")){
            holder.txt_label2.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_initiated));
            holder.txt_label8.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_initiated));
        }else{
            holder.txt_label2.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_success));
            holder.txt_label8.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_success));
        }*/
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
