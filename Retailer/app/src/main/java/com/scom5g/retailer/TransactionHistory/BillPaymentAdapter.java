package com.scom5g.retailer.TransactionHistory;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Scom5GRetailer;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BillPaymentAdapter extends RecyclerView.Adapter<BillPaymentAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String frg_name = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtOperatorName,txtAmount,txtConsumerId,txtBillPaymentId,txtCustomerPhone,txtSurcharge,txtBillerTransactionId,txtremark,txttime,txtStatus;
        ImageView img_arrow;
        LinearLayout linear_more_data;

        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txtOperatorName = (TextView) view.findViewById(R.id.txtOperatorName);
            txtAmount = (TextView) view.findViewById(R.id.txtAmount);
            txtConsumerId = (TextView) view.findViewById(R.id.txtConsumerId);
            txtBillPaymentId = (TextView) view.findViewById(R.id.txtBillPaymentId);
            txtCustomerPhone = (TextView) view.findViewById(R.id.txtCustomerPhone);
            txtSurcharge = (TextView) view.findViewById(R.id.txtSurcharge);
            txtBillerTransactionId = (TextView) view.findViewById(R.id.txtBillerTransactionId);
            txtremark = (TextView) view.findViewById(R.id.txtremark);
            txttime = (TextView) view.findViewById(R.id.txttime);
            txtStatus = (TextView) view.findViewById(R.id.txtStatus);
            img_arrow = (ImageView) view.findViewById(R.id.img_arrow);
            linear_more_data = (LinearLayout) view.findViewById(R.id.linear_more_data);

            img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility = linear_more_data.getVisibility();
                    if (visibility == View.VISIBLE) {
                        linear_more_data.setVisibility(View.GONE);
                        img_arrow.setImageResource(R.drawable.ic_expand_more);
                    } else {
                        linear_more_data.setVisibility(View.VISIBLE);
                        img_arrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });
        }
    }

    public BillPaymentAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public BillPaymentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bill_payment_layout, parent, false);

        return new BillPaymentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BillPaymentAdapter.MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txtOperatorName.setText(movie.getLabel11() + "");
        holder.txtAmount.setText(movie.getLabel12() + "");
        holder.txtConsumerId.setText("Consumer Id: " + movie.getLabel13());
        holder.txtBillPaymentId.setText("Bill Payment Id: " + movie.getLabel14() + "");
        holder.txtCustomerPhone.setText("Customer No: "+movie.getLabel15() + "");
        holder.txtSurcharge.setText("Surcharge: " + movie.getLabel16() + "");
        holder.txtBillerTransactionId.setText("Biller Transaction Id: "+movie.getLabel17() + "");
        holder.txtremark.setText("Remark: "+movie.getLabel18() + "");
        holder.txttime.setText(movie.getLabel19() +"");
        holder.txtStatus.setText(movie.getLabel20() + "");

        if (movie.getLabel20().equalsIgnoreCase("Failed")) {
            holder.txtAmount.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(), R.color.status_fail));
            holder.txtStatus.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(), R.color.status_fail));
        } else if (movie.getLabel20().equalsIgnoreCase("Pending")) {
            holder.txtAmount.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(), R.color.status_initiated));
            holder.txtStatus.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(), R.color.status_initiated));
        } else {
            holder.txtAmount.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(), R.color.status_success));
            holder.txtStatus.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(), R.color.status_success));
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
