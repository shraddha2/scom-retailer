package com.scom5g.retailer.Recharge;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.scom5g.retailer.HomePage.HomeFragment;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;

import java.util.ArrayList;
import java.util.List;

public class MobileOperatorFragment extends Fragment {

    GridView gridView;
    private List<Item> rp_data;
    String trans_type="";
    TextView txt_title,txt_sub_title;
    //Typeface font ;
    public MobileOperatorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView= inflater.inflate(R.layout.fragment_mobile_operator, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        Bundle bundle = getArguments();
        trans_type = bundle.getString("trans_type");
        if(trans_type.equalsIgnoreCase("Mobile Prepaid")) {
            txt_title.setText("Prepaid Recharge (Choose Operator)");
        }else   if(trans_type.equalsIgnoreCase("Mobile Postpaid")) {
            txt_title.setText("Postpaid Recharge (Choose Operator)");
        }
        if(trans_type.equalsIgnoreCase("Mobile Prepaid")) {
            rp_data = fill_with_data2();
        }else if(trans_type.equalsIgnoreCase("Mobile Postpaid"))
        {
            rp_data = fill_with_data3();
        }else{

        }
        // Instance of ImageAdapter Class
        GridView gridView = (GridView) rootView.findViewById(R.id.gridview);
        GridRechargeAdapter2 booksAdapter = new GridRechargeAdapter2(rp_data, getActivity());
        gridView.setAdapter(booksAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String operator_name = rp_data.get(position).txt.toString();
                String operator_id = rp_data.get(position).txt_id.toString();
                Bundle bundle=new Bundle();
                bundle.putString("operator_name", operator_name);
                bundle.putString("operator_id", operator_id);
                if(trans_type.equalsIgnoreCase("Mobile Prepaid")) {
                    android.app.Fragment frg = new PrepaidRechargeFragment();
                    frg.setArguments(bundle);
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Prepaid")
                            .addToBackStack(null)
                            .commit();
                }else   if(trans_type.equalsIgnoreCase("Mobile Postpaid")) {
                    android.app.Fragment frg = new PostpaidRechargeFragment();
                    frg.setArguments(bundle);
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Postpaid")
                            .addToBackStack(null)
                            .commit();
                }

            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    public List<Item> fill_with_data2() {

        List<Item> data = new ArrayList<>();
        data.add(new Item( R.drawable.ic_airtel, "Airtel","1"));
        data.add(new Item( R.drawable.ic_aircel, "Aircel","2"));
        data.add(new Item( R.drawable.ic_idea_logo, "Idea","3"));
        data.add(new Item( R.drawable.ic_vodafone, "Vodafone","4"));
        data.add(new Item( R.drawable.ic_bsnl, "Bsnl-Topup","5"));
        data.add(new Item( R.drawable.ic_bsnl, "Bsnl-STV","6"));
        data.add(new Item( R.drawable.ic_telenor, "Telenor-Topup","7"));
        data.add(new Item( R.drawable.ic_telenor, "Telenor-Special","8"));
        data.add(new Item( R.drawable.ic_jio, "Jio","9"));
        data.add(new Item( R.drawable.ic_docomo, "Docomo-Topup","10"));
        data.add(new Item( R.drawable.ic_docomo, "Docomo-Special","11"));
        data.add(new Item( R.drawable.ic_mtnl, "Mtnl-Topup","12"));
        data.add(new Item( R.drawable.ic_mtnl, "Mtnl-Special","13"));
        return data;
    }

    public List<Item> fill_with_data3() {

        List<Item> data = new ArrayList<>();
        data.add(new Item( R.drawable.ic_airtel, "Airtel-Postpaid","14"));
        data.add(new Item( R.drawable.ic_idea_logo, "Idea-Postpaid","15"));
        data.add(new Item( R.drawable.ic_vodafone, "Vodafone-Postpaid","16"));
        data.add(new Item( R.drawable.ic_aircel, "Aircel-Postpaid","17"));
        data.add(new Item( R.drawable.ic_docomo, "Docomo-Postpaid","18"));
        return data;
    }

    @Override
    public void onResume() {

        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;
                }
                return false;
            }
        });
    }

}
