package com.scom5g.retailer.SendMoney;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Scom5GRetailer;
import com.scom5g.retailer.TransactionHistory.AddComplainActivity;
import com.scom5g.retailer.TransactionHistory.RechargeAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MtSuccessAdapter extends RecyclerView.Adapter<MtSuccessAdapter.MyViewHolder> {

    private List<Item> TransactionList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTransaction,txtTransactionId,txtAmount,txtBankRefNo,txtStatus;

        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txtTransaction = (TextView) view.findViewById(R.id.txtTransaction);
            txtTransactionId = (TextView) view.findViewById(R.id.txtTransactionId);
            txtAmount = (TextView) view.findViewById(R.id.txtAmount);
            txtBankRefNo = (TextView) view.findViewById(R.id.txtBankRefNo);
            txtStatus = (TextView) view.findViewById(R.id.txtStatus);
        }
    }

    public MtSuccessAdapter(List<Item> TransactionList, Context ctx, String frg_name) {
        this.TransactionList = TransactionList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public MtSuccessAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mt_success_layout, parent, false);

        return new MtSuccessAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MtSuccessAdapter.MyViewHolder holder, int position) {
        Item movie = TransactionList.get(position);

        holder.txtTransaction.setText(movie.getLabel1()+"");
        holder.txtTransactionId.setText(movie.getLabel2()+"");
        holder.txtAmount.setText(movie.getLabel3()+"");
        holder.txtBankRefNo.setText(movie.getLabel4()+"");
        holder.txtStatus.setText(movie.getLabel5()+"");

        if (movie.getLabel5().equalsIgnoreCase("Failed")) {
            holder.txtAmount.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_fail));
            holder.txtStatus.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_fail));
        } else if (movie.getLabel5().equalsIgnoreCase("Pending")){
            holder.txtAmount.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_initiated));
            holder.txtStatus.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_initiated));
        }else{
            holder.txtAmount.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_success));
            holder.txtStatus.setTextColor(ContextCompat.getColor(Scom5GRetailer.getInstance(),R.color.status_success));
        }
    }

    @Override
    public int getItemCount() {
        return TransactionList.size();
    }
}
