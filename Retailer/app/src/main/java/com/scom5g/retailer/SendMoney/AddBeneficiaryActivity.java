package com.scom5g.retailer.SendMoney;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;
import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.LastTenTransaction.CustomSpinnerAdapter;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;
import com.scom5g.retailer.Scom5GRetailer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddBeneficiaryActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    String cust_id="", sender_no="", senderName="";
    EditText edt_fname,edt_lname,edt_mobile_no,edt_acct_no,edt_bank,edt_ifsc;
    Button btn_submit,btn_verify;
    String smobno,fname,lname,acct_no,bank_id="0",bank_name="",ifsc,mobile_no;
    SharedPreferences sharedpreferences;
    String token;
    String cust_mobile;
    TextView txt_error,txt_label10;
    List<Item> list_banks;
    CustomSpinnerAdapter sp_adapter = null;
    LinearLayout linear_ifsc;
    LinearLayout progress_linear,container;
    ProgressBar prg;
    private String limit, cust_name;
    private String beneficiary_id="";
    String verifyAcServerSideError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_beneficiary);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cust_id=getIntent().getStringExtra("cust_id");
        cust_mobile=getIntent().getStringExtra("mobile_no");
        limit=getIntent().getStringExtra("limit");
        cust_name=getIntent().getStringExtra("cust_name");

        list_banks=new ArrayList<>();
        String mess = getResources().getString(R.string.app_name);
        setTitle(mess);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Scom5GRetailer.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        prg=(ProgressBar) findViewById(R.id.prg);
        edt_bank=(EditText) findViewById(R.id.edt_bank);
        edt_mobile_no=(EditText) findViewById(R.id.edt_mobile_number);
        edt_fname=(EditText) findViewById(R.id.edt_fname);
        edt_lname=(EditText) findViewById(R.id.edt_lname);
        edt_ifsc=(EditText) findViewById(R.id.edt_ifsc);
        edt_acct_no=(EditText) findViewById(R.id.edt_acct_no);
        linear_ifsc=(LinearLayout) findViewById(R.id.linear_ifsc);
        progress_linear= (LinearLayout) findViewById(R.id.loding);
        container= (LinearLayout) findViewById(R.id.container);
        txt_error=(TextView)findViewById(R.id.txt_error);
        btn_submit=(Button) findViewById(R.id.btn_submit);
        btn_verify=(Button) findViewById(R.id.btn_verify);
        txt_label10=(TextView)findViewById(R.id.txt_label10);
        progress_linear.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);

        edt_bank.setInputType(InputType.TYPE_NULL);
        edt_bank.setFocusableInTouchMode(false);
        edt_bank.setFocusable(false);

        edt_acct_no.setVisibility(View.GONE);
        txt_label10.setVisibility(View.GONE);

        edt_bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AddBeneficiaryActivity.this,BankNameListActivity.class);
                intent.putExtra("source","rech");
                startActivityForResult(intent, 3);
            }
        });

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkConnection()) {

                    acct_no=edt_acct_no.getText().toString();
                    ifsc=edt_ifsc.getText().toString();
                    mobile_no=edt_mobile_no.getText().toString();
                    fname=edt_fname.getText().toString();

                    boolean isError=false;

                    if(linear_ifsc.getVisibility()==View.VISIBLE)
                    {
                        //final String ifsc = edt_ifsc.getText().toString();
                        if (!isValidifsc(ifsc)) {
                            edt_ifsc.setError("Field Cannot be Blank");
                            isError=true;
                        }
                    } else{
                        isError=false;
                    }

                    if(null==fname||fname.length()==0||fname.equalsIgnoreCase("")|| isValidName(fname))
                    {
                        isError=true;
                        edt_fname.setError("Enter First and Last Name");
                    }

                    if(null==acct_no||acct_no.length()==0||acct_no.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_acct_no.setError("Field Cannot be Blank");
                    }

                    if(null==bank_id||bank_id.length()==0||bank_id.equalsIgnoreCase("0"))
                    {
                        isError=true;
                        edt_bank.setError("Select bank name");
                    }

                    if (!isError) {

                        final AlertDialog.Builder builder = new AlertDialog.Builder(AddBeneficiaryActivity.this);
                        builder.setMessage("You will charge Rs. 5 for this verification");
                        builder.setPositiveButton("Verify",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        Log.d("msg","before verify a/c");

                                        new VerifyAccount().execute();

                                    }
                                });
                        builder.setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.show();
                    }

                } else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("No Internet Connection");
                }
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*ifsc="";*/
                if (checkConnection()) {
                    mobile_no=edt_mobile_no.getText().toString();
                    fname=edt_fname.getText().toString();
                    acct_no=edt_acct_no.getText().toString();
                    ifsc = edt_ifsc.getText().toString();
                    boolean isError=false;
                    if(linear_ifsc.getVisibility()==View.VISIBLE)
                    {
                        //final String ifsc = edt_ifsc.getText().toString();
                        if (!isValidifsc(ifsc)) {
                            edt_ifsc.setError("Field Cannot be Blank");
                            isError=true;
                        }
                    } else{
                        isError=false;
                    }
                    if(null == mobile_no || mobile_no.length() == 0 || mobile_no == "" || mobile_no.length() != 10)
                    {
                        isError=true;
                        edt_mobile_no.setError("Field Cannot be Blank");
                    }
                    if(null==fname||fname.length()==0||fname.equalsIgnoreCase("")|| isValidName(fname))
                    {
                        isError=true;
                        edt_fname.setError("Enter First and Last Name");
                    }
                    if(null==acct_no||acct_no.length()==0||acct_no.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_acct_no.setError("Field Cannot be Blank");
                    }
                    if(null==bank_id||bank_id.length()==0||bank_id.equalsIgnoreCase("0"))
                    {
                        isError=true;
                        edt_bank.setError("Select bank name");
                    }
                    if (!isError) {
                        new AddBeneficiary().execute();
                    }

                } else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("No Internet Connection");
                }
            }
        });

    }
    public final static boolean isValidName(String target) {
        return Pattern.compile("^(?=.*[a-zA-Z가-힣])[a-zA-Z가-힣]{1,}$").matcher(target).matches();

    }

    private boolean isValidifsc(String ifsc) {
        if (ifsc == null || ifsc.length() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(progress_linear.getVisibility()==View.GONE) {
                    onBackPressed();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Add beneficiary
    private class AddBeneficiary extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("senderId", cust_id);
                parameters.put("senderMobileNo", cust_mobile);
                parameters.put("benMobNum", mobile_no);
                parameters.put("ifscCode", ifsc);
                parameters.put("beneficiaryName", fname);
                parameters.put("bankId", bank_id);
                parameters.put("accountNo", acct_no);
                parameters.put("bankFullName", bank_name);
                //Log.d("addcusttype1", response + " "+ sender_no + "" + fname+" "+lname+" " + mobile_no + "" + ifsc + "" + acct_no +"" + cust_id);

                this.response = new JSONObject(service.POST(DefineData.ADD_BENEFICIARY,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            //Log.d("fgf", response + " "+ cust_id + " "+cust_mobile+ " " + mobile_no+" "+ifsc+" " + fname + " " + bank_id +" " + bank_name + " "+ acct_no + " ");
            FirebaseCrash.log(response+"");

            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                    }
                    if (responseCode==4)
                    {
                        msg = response.getString("responseDesc");
                        txt_error.setText(msg);
                        txt_error.setVisibility(View.VISIBLE);
                    }
                    if (responseCode==25)
                    {
                        msg = response.getString("responseDesc");
                        txt_error.setText(msg);
                        txt_error.setVisibility(View.VISIBLE);
                    }
                    if (responseCode==1)
                    {
                        txt_error.setVisibility(View.GONE);
                        container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        beneficiary_id=response.getString("beneficiaryId");

                        Intent i=new Intent(AddBeneficiaryActivity.this, SendMoneyActivity.class);
                        i.putExtra("cust_id",cust_id);
                        i.putExtra("beneficiaryId",beneficiary_id);
                        i.putExtra("accountNo",acct_no);
                        i.putExtra("beneficiaryName",fname.replaceAll(" ",""));
                        i.putExtra("bankName",bank_name);
                        i.putExtra("sender_no",cust_mobile);
                        i.putExtra("ifsc",ifsc);
                        i.putExtra("neftLimitRs",limit);
                        i.putExtra("senderName",cust_name);
                      /*  Log.d("AddBenfsendMoneyhhh",   cust_id + " " + beneficiary_id+" "+acct_no+" " + fname + " " + bank_name +" " +
                                cust_mobile + " "+ ifsc + " "+ limit+" "+ cust_name+" ");*/
                        startActivity(i);
                        finish();

                    } else if (responseCode==101)
                    {
                        StringBuilder sb = new StringBuilder();

                        msg = response.getString("responseDesc");
                       //Log.d("tytruytru","i m in response 101");
                        txt_error.setText(msg);
                        txt_error.setVisibility(View.VISIBLE);
                        container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                    }else
                    {
                        txt_error.setText(msg);
                        txt_error.setVisibility(View.VISIBLE);
                        container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("Something went wrong! Erroe Code-1");
                    container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_error.setVisibility(View.VISIBLE);
                txt_error.setText("Something went wrong!  Erroe Code-2");
                container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    //Check is IFSC  code is required for the selected bank name
    private class CheckIFSC extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            prg.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("bankId", bank_id);
                this.response = new JSONObject(service.POST(DefineData.CHECK_IFSC,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            FirebaseCrash.log(response+"");
            prg.setVisibility(View.GONE);
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        ifsc="";
                    } else {
                        String otp_required=response.getString("ifsc_required");
                        if(otp_required.equalsIgnoreCase("yes"))
                        {
                            linear_ifsc.setVisibility(View.VISIBLE);

                        }
                        if(otp_required.equalsIgnoreCase("no"))
                        {
                            linear_ifsc.setVisibility(View.GONE);
                        }
                        edt_acct_no.setVisibility(View.VISIBLE);
                        txt_label10.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{

            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
    @Override
    protected void onResume() {
        super.onResume();
        // register connection status listener
        Scom5GRetailer.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyboard(AddBeneficiaryActivity.this);
        finish();
    }

    //Verify bank account details if required before transferring money
    private class VerifyAccount extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            txt_error.setText("");
            txt_error.setVisibility(View.GONE);
            container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("beneficiary_ifsc", ifsc);
                parameters.put("sender_id", cust_mobile);
                parameters.put("beneficiary_name", fname);
                parameters.put("beneficiary_number", mobile_no);
                parameters.put("beneficiary_bank_id", bank_id);
                parameters.put("beneficiary_bank_account", acct_no);
                parameters.put("token", token);
                this.response = new JSONObject(service.POST(DefineData.VERIFY_ACCOUNT,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            //Log.d("accountverify", response + " "+ ifsc + " " + cust_mobile+" "+ifsc+" " + fname + " " + mobile_no +" " + bank_id + " "+ acct_no+ " ");
            if(response!=null) {
                try {
                    if(response.getBoolean("error"))
                    {
                        String msg="";
                        if(response.has("responseDesc")) {
                            msg = response.getString("responseDesc");
                        }
                        txt_error.setText(msg+"");
                        verifyAcServerSideError=msg;
                        txt_error.setVisibility(View.VISIBLE);
                        //txt_error.setText("Error in Verifying account");
                        container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);

                        /*boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                    }*/
                    }else{
                        if(!response.getBoolean("error")) {
                            txt_error.setVisibility(View.GONE);
                            txt_error.setText("");
                            container.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                            final String benename=response.getString("beneficiary_name");
                            String status="";

                            final Dialog dialog = new Dialog(AddBeneficiaryActivity.this);
                            dialog.setContentView(R.layout.verify_beneficiary);
                            TextView txt_entered_bname=dialog.findViewById(R.id.txt_entered_bname);
                            TextView txt_fetch_bname=dialog.findViewById(R.id.txt_fetch_bname);
                            TextView txt_acctno=dialog.findViewById(R.id.txt_acctno);

                            fname=fname.toUpperCase();

                            txt_entered_bname.setText(fname+"");
                            txt_fetch_bname.setText(benename+"");
                            txt_acctno.setText(acct_no+"");

                            Button btn_yes=dialog.findViewById(R.id.btn_yes);
                            Button btnno=dialog.findViewById(R.id.btnno);

                            // if button is clicked, close the custom dialog
                            btn_yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    edt_fname.setText(benename + "");

                                }
                            });
                            // if button is clicked, close the custom dialog
                            btnno.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();

                                }
                            });
                            dialog.show();
                        }else{
                            String msg=response.getString("message");
                            txt_error.setVisibility(View.VISIBLE);
                            txt_error.setText(msg+" ");
                            container.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }
                    }

                }  catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setVisibility(View.VISIBLE);
                    //txt_error.setText("Something went wrong");
                    txt_error.setText(verifyAcServerSideError+"");
                    container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_error.setVisibility(View.VISIBLE);
                txt_error.setText("Something went wrong on server");
                container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case 3:
                if (null!=data) {
                    bank_id = data.getStringExtra("BankId");
                    bank_name = data.getStringExtra("BankName");
                    edt_bank.setText(bank_name + "");
                    //Log.d("bankid",bank_id);
                    new CheckIFSC().execute();
                }
                break;
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
