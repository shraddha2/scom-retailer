package com.scom5g.retailer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DefineData {

    public static final String LOGIN_MINKSPAY_PREFERENCE = "loginscom5gpref" ;
    public static final String TOKEN_KEY = "token_key";
    public static final String KEY_LOGIN_SUCCESS = "key_login";
    public static final String KEY_REMEMBER_ME = "remember_me_key";
    public static final String KEY_RETAILER_PHONENO = "key_retailer_phoneno";
    public static final String KEY_RETAILER_NAME = "key_retailer_name";
    public static final String KEY_WALLET_BALANCE = "key_wallet_balance";
    public static final String KEY_USER_NAME = "key_username";
    public static final String KEY_PASSWORD = "key_password";
    public static final String KEY_REMEMBER_PIN = "remember_pin_key";
    public static final String REGISTERED_MOBILE_NO = "registered_mobile_no";
    public static final String CUSTOMER_MOBILE_NO = "customer_mobile_no";

    public static final String KEY_PERCENTAGE = "key_percentage_data";

    //offline
    public static final String KEY_OPERATOR_CIRCLE_OFFLINE = "key_operator_circle_offline";
    public static final String KEY_IS_CIRCLE_SAVE_OFFLINE = "key_oc_save_offline";
    public static final String KEY_OPERATOR_CIRCLE_ID_OFFLINE = "key_oc_save_id_offline";

   //prepaid
    public static final String KEY_OPERATOR_CIRCLE = "key_operator_circle";
    public static final String KEY_IS_CIRCLE_SAVE = "key_oc_save";
    public static final String KEY_OPERATOR_CIRCLE_ID = "key_oc_save_id";

    //postpaid
    public static final String KEY_OPERATOR_CIRCLE_PP = "key_operator_circle_pp";
    public static final String KEY_IS_CIRCLE_SAVE_PP  = "key_oc_save_pp";
    public static final String KEY_OPERATOR_CIRCLE_ID_PP = "key_oc_save_id_pp";

    //DTH
    public static final String KEY_OPERATOR_CIRCLE_DTH = "key_operator_circle_dth";
    public static final String KEY_IS_CIRCLE_SAVE_DTH  = "key_oc_save_dth";
    public static final String KEY_OPERATOR_CIRCLE_ID_DTH= "key_oc_save_id_dth";

    public static final String OFFLINE_MOBILE_NUMBER = "9970822776";

    //http://192.168.0.106/scom_localhost/public/api/retailer/login
  /*  private static final String prefix="http://192.168.1.107/scom_localhost/public/";
    private static final String mid_prefix="api/retailer/";
    public static final String CHECK_VERSION="http://192.168.1.107/scom_localhost/public/api/validate-version";*/

 /*  public static final String GENERATE_OTP ="http://test-app.scomrecharge.com/api/user/login-send-otp";
    public static final String AUTHENTICATE_OTP ="http://test-app.scomrecharge.com/api/user/validate-otp";*/

    //private static final String prefix="http://test-app.scomrecharge.com/";
    //https://app.scomrecharge.com/
    private static final String prefix="https://app.scomrecharge.com/";
    private static final String mid_prefix="api/retailer/";
    public static final String CHECK_VERSION=prefix+"api/validate-version";
    public static final String GENERATE_OTP =prefix+"api/user/login-send-otp";
    public static final String AUTHENTICATE_OTP =prefix+"api/user/validate-otp";
    public static final String LOGIN_URL =prefix+mid_prefix+"login";
    public static final String RECHARGE_URL =prefix+mid_prefix+"recharge";
    public static final String SEARCH_RECHARGE =prefix+mid_prefix+"search-recharge";
    public static final String SEARCH_RECHARGE_HISTORY =prefix+mid_prefix+"recharge-history";
    public static final String SEARCH_FUND_REQUEST =prefix+mid_prefix+"get-banks";
    public static final String SEARCH_DISTRIBUTOR_BANK_DETAILS =prefix+mid_prefix+"view-distributor-bank-details";

    public static final String SEARCH_CUSTOMER_DEFAULT =prefix+mid_prefix+"get-customer-list";

    public static final String GET_BANK_LIST=prefix+mid_prefix+"dmtBankList";
    public static final String GET_BENEFICIARY_LIST=prefix+mid_prefix+"viewBeneficiary";
    public static final String GET_MT_LAST_TRANS=prefix+mid_prefix+"money-transfer-latest";
    public static final String GET_MT_TRANS_HIST=prefix+mid_prefix+"money-transfer-history";
    public static final String SEARCH_ACCOUNT_NUMBER=prefix+mid_prefix+"search-account-no";
    public static final String ADD_CUSTOMER=prefix+mid_prefix+"registerCustomer";
    public static final String CONFIRM_CUSTOMER1=prefix+mid_prefix+"registerCustomerConfirm";
    public static final String CONFIRM_CUSTOMER2=prefix+mid_prefix+"registerCustomerConfirm";
    public static final String FETCH_DETAILS=prefix+mid_prefix+"chkBenFees";
    public static final String SEND_MONEY=prefix+mid_prefix+"sendMoney";
    public static final String FETCH_PERSONAL_DETAILS=prefix+mid_prefix+"fetch-edit-personal";
    public static final String EDIT_PERSONAL_DETAILS =prefix+mid_prefix+"edit-personal";
    public static final String FETCH_KYC_DETAILS=prefix+mid_prefix+"fetch-edit-kyc";
    public static final String EDIT_KYC_DETAILS =prefix+mid_prefix+"edit-kyc";
    public static final String FETCH_PROFIT_DETAILS =prefix+mid_prefix+"business-report";
    public static final String FETCH_PERCENTAGE_DATA =prefix+mid_prefix+"pie-chart/show-sales";
    public static final String SEARCH_DTHBOOKING_HISTORY=prefix+mid_prefix+"getDthBookingHistory";

    public static final String SEARCH_CUSTOMER =prefix+mid_prefix+"viewBeneficiary";
    public static final String RESEND_OTP =prefix+mid_prefix+"resendOtp";
    public static final String VERIFY_OTP =prefix+mid_prefix+"registerCustomerConfirm";
    public static final String ADD_BENEFICIARY=prefix+mid_prefix+"addBeneficiary";
    public static final String CHECK_IFSC=prefix+mid_prefix+"verifyIfsc";
    public static final String VERIFY_ACCOUNT=prefix+mid_prefix+"verifyAccount";
    public static final String DELETE_BENEFICIARY=prefix+mid_prefix+"disableBeneficiary";

    //Browse Plan
    public static final String VIEW_BROWSE_PLAN=prefix+mid_prefix+"viewSimplePlans";
    public static final String VIEW_SPECIAL_PLAN=prefix+mid_prefix+"viewSpecialPlans";
    public static final String VIEW_DTH_CUSTOMER_INFO=prefix+mid_prefix+"viewDthInfo";
    public static final String VIEW_DTH_PLANS=prefix+mid_prefix+"viewDthPlans";
    public static final String CHECK_VALIDITY=prefix+mid_prefix+"checkRechargeNoValidity";

    public static final String GET_BILLERS=prefix+mid_prefix+"retailer-pay-electricity-bill-get-billers";
    public static final String GET_DYNAMIC_FIELDS=prefix+mid_prefix+"retailer-pay-electricity-bill-get-customer-parameters";
    public static final String GET_BILL_DETAILS=prefix+mid_prefix+"retailer-pay-electricity-bill-send-electricity-bill-fetch-request";
    public static final String BILL_PAY=prefix+mid_prefix+"retailer-pay-electricity-bill-send-electricity-bill-pay-request";
    public static final String FETCH_COMMISIONS=prefix+mid_prefix+"get-commission-chart";
    public static final String FETCH_COMMISIONS_REPORT=prefix+mid_prefix+"get-commission-report";
    public static final String FETCH_LATEST_TRANSACTIONS=prefix+mid_prefix+"latest-recharges";
    public static final String BILL_HISTORY=prefix+mid_prefix+"retailer-get-bill-payment-history";
    public static final String FETCH_PROFILE=prefix+mid_prefix+"get-profile";
    public static final String SEND_COMPLAINT=prefix+mid_prefix+"add-complaint";
    public static final String FETCH_COMPLAINT=prefix+mid_prefix+"get-complaint-list";
    public static final String UPDATE_PASSWORD=prefix+mid_prefix+"change-password";
    public static final String OTP_FORGOT_PASSWORD=prefix+mid_prefix+"forgot-password-otp";
    public static final String FORGOT_PASSWORD=prefix+mid_prefix+"forgot-password";
    public static final String FETCH_WALLET_HISTORY=prefix+mid_prefix+"get-wallet-history";
    public static final String MT_REFUND_MONEY_TRANSFER=prefix+mid_prefix+"money-transfer-refund";
    public static final String MT_REFUND_SUBMIT_OTP=prefix+mid_prefix+"money-transfer-refund-otp";
    public static final String FETCH_BALANCE=prefix+mid_prefix+"get-balance";
    public static final String FETCH_PERCENTAGE=prefix+mid_prefix+"get-percentage-sales";
    public static final String OLD_DATA_URL="http://13.126.134.145/retailer/login";
    public static final String FETCH_ACCOUNT_REPORT=prefix+mid_prefix+"get-account-ledger";
    public static final String FETCH_OUTSTANDING_REPORT=prefix+mid_prefix+"get-payment-report";
    public static final String SUBMIT_DETAILS=prefix+mid_prefix+"request-funds-submit";
    public static final String MY_FUND_REQUEST=prefix+mid_prefix+"view-sent-fund-requests";
    public static final String CHECK__PENDING_STATUS=prefix+mid_prefix+"money-transfer-check-transaction";

    //Bill Payment
    public static final String GENERATE_BILL=prefix+mid_prefix+"fetch-bill";
    public static final String CHECK_BBPS_STATUS=prefix+mid_prefix+"chkBbpsStatus";
    public static final String SEND_OTP=prefix+mid_prefix+"bbps-send-otp";
    public static final String REGISTER_RETAILER=prefix+mid_prefix+"registerForBbps";
    public static final String PAY_BILL=prefix+mid_prefix+"pay-bill";
    public static final String BILL_PAYMENT_HISTORY=prefix+mid_prefix+"bill-payment-reports/transaction-history";
    public static final String BILL_PAYMENT_LAST_TRANSACTION=prefix+mid_prefix+"bill-payment-reports/latest-bill-payments";

    public static final String OTP_UPDATE_PASSWORD=prefix+mid_prefix+"change-password-otp";

    public static final String FETCH_VERSION=prefix+"retailer-get-current-app-version";

    //AePS
    public static final String GET_AEPS_KEY=prefix+mid_prefix+"get-aeps-keys";
    public static final String AEPS_HISTORY=prefix+mid_prefix+"get-aeps-transaction-report";
    public static final String AEPS_LAST_TEN_TRANSACTION=prefix+mid_prefix+"get-latest-aeps-transaction";

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    public static String parseDateToyyyMMdd(String time) {
        String outputPattern = "yyyy-MM-dd";
        String inputPattern= "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseDateToddMMyyyyhh(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy hh:mm:ss aa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd ");
        String strDate = mdformat.format(calendar.getTime());

       return strDate;
    }
}