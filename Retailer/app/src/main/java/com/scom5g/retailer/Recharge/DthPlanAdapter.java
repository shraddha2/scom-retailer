package com.scom5g.retailer.Recharge;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;

import java.util.List;

public class DthPlanAdapter extends RecyclerView.Adapter<DthPlanAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_plan_name,txt_description,txt_last_update,oneMonth,threeMonth,sixMonth,oneYear;
        Button btn2,btn3,btn4;
        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txt_plan_name = (TextView) view.findViewById(R.id.txt_plan_name);
            txt_description = (TextView) view.findViewById(R.id.txt_description);
            txt_last_update = (TextView) view.findViewById(R.id.txt_last_update);
            oneMonth = (TextView) view.findViewById(R.id.oneMonth);
            threeMonth = (TextView) view.findViewById(R.id.threeMonth);
            sixMonth = (TextView) view.findViewById(R.id.sixMonth);
            oneYear = (TextView) view.findViewById(R.id.oneYear);

            oneMonth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String amount=oneMonth.getText().toString();
                    Intent intent=new Intent();
                    intent.putExtra("amount",amount);
                    ((Activity) ctx).setResult(22,intent);
                    ((Activity) ctx).finish();
                }
            });
            threeMonth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String amount=threeMonth.getText().toString();
                    Intent intent=new Intent();
                    intent.putExtra("amount",amount);
                    ((Activity) ctx).setResult(22,intent);
                    ((Activity) ctx).finish();
                }
            });
            sixMonth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String amount=sixMonth.getText().toString();
                    Intent intent=new Intent();
                    intent.putExtra("amount",amount);
                    ((Activity) ctx).setResult(22,intent);
                    ((Activity) ctx).finish();
                }
            });
            oneYear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String amount=oneYear.getText().toString();
                    Intent intent=new Intent();
                    intent.putExtra("amount",amount);
                    ((Activity) ctx).setResult(22,intent);
                    ((Activity) ctx).finish();
                }
            });
        }
    }

    public DthPlanAdapter(List<Item> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public DthPlanAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dth_plan_layout, parent, false);

        return new DthPlanAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DthPlanAdapter.MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);
        holder.txt_plan_name.setText(movie.getTrans_no()+"");
        holder.txt_description.setText(movie.getTrans_type()+"");
        holder.txt_last_update.setText(movie.getTrans_amount()+"");
        holder.oneMonth.setText(movie.getTrans_datetime()+"");
        holder.threeMonth.setText(movie.getTrans_update_balance()+"");
        holder.sixMonth.setText(movie.getStatus()+"");
        holder.oneYear.setText(movie.getP_name()+"");
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
