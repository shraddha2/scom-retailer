package com.scom5g.retailer.BillPayment;

public class BillData {

    public int imageId;
    public String txt,txt_id;

    public int getImageId() {
        return imageId;
    }
    public String getTxt() {
        return txt;
    }
    public String getTxt_id() {
        return txt_id;
    }

    public BillData(int imageId, String text, String txt_id) {
        this.imageId = imageId;
        this.txt=text;
        this.txt_id=txt_id;
    }
}
