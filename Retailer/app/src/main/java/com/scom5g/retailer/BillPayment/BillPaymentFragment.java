package com.scom5g.retailer.BillPayment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.FundRequest.FundRequestFragment;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.NavigationDrawer.HomeActivity;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;
import com.scom5g.retailer.Scom5GRetailer;
import com.scom5g.retailer.SendMoney.AddBeneficiaryActivity;
import com.scom5g.retailer.SendMoney.SendMoneyActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission.SEND_SMS;

public class BillPaymentFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener{

    TextView txtSelectOperator,textView19,txtError,txtCheckTime,txtRegisterError, textViewBillingUnit,textViewConsumerId;
    EditText edtConsumerIdBbps,edtCustomerMobileNoBbps,edtBillingUnit,edtEnterOTP,edtPanCard;
    Button btnContinue,btnSendOtp,btnRegister;
    String ConsumerIdBbps,enterOTP, panCard, CustomerMobileNoBbps,BillingUnit,operator_id="",operator_name="",token,backendError,registerError,storeLatitude,storeLongitude;
    SharedPreferences sharedpreferences;
    public int counter;
    ConstraintLayout linearLayout,checkStatus,loading, linearLayoutExtra,content;
    GPSTracker gps;
    Context mContext;
    String storeAmount,storeSurcharge,storeTotalAmount, referenceID,selectOperator, storeCustomerName;

    public BillPaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bill, container, false);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Scom5GRetailer.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        mContext = getActivity();

        content = (ConstraintLayout) rootView.findViewById(R.id.container);
        textView19= (TextView) rootView.findViewById(R.id.textView19);
        edtConsumerIdBbps= (EditText) rootView.findViewById(R.id.edtConsumerIdBbps);
        edtCustomerMobileNoBbps= (EditText) rootView.findViewById(R.id.edtCustomerMobileNoBbps);
        edtBillingUnit= (EditText) rootView.findViewById(R.id.edtBillingUnit);
        edtEnterOTP= (EditText) rootView.findViewById(R.id.edtEnterOTP);
        edtPanCard= (EditText) rootView.findViewById(R.id.edtPanCard);
        txtError= (TextView) rootView.findViewById(R.id.txtError);
        txtRegisterError= (TextView) rootView.findViewById(R.id.txtRegisterError);
        linearLayout= (ConstraintLayout) rootView.findViewById(R.id.linearLayout);
        checkStatus= (ConstraintLayout) rootView.findViewById(R.id.checkStatus);
        checkStatus.setVisibility(View.GONE);

        textViewBillingUnit= (TextView) rootView.findViewById(R.id.textViewBillingUnit);
        textViewConsumerId= (TextView) rootView.findViewById(R.id.textViewConsumerId);

        linearLayoutExtra= (ConstraintLayout) rootView.findViewById(R.id.linearLayoutExtra);
        linearLayoutExtra.setVisibility(View.GONE);

        loading= (ConstraintLayout) rootView.findViewById(R.id.loading);

        txtSelectOperator= (TextView) rootView.findViewById(R.id.txtSelectOperator);
        txtSelectOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),BBPSOperatotListActivity.class);
                startActivityForResult(intent, 5);
            }
        });

        txtCheckTime= (TextView) rootView.findViewById(R.id.txtCheckTime);
        txtCheckTime.setVisibility(View.GONE);
        btnSendOtp= (Button) rootView.findViewById(R.id.btnSendOtp);
        btnSendOtp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                new SendOTP().execute();
                new CountDownTimer(30000, 1000){
                    public void onTick(long millisUntilFinished){
                        txtCheckTime.setVisibility(View.VISIBLE);
                        txtCheckTime.setText("Resend OTP after "+String.valueOf(counter)+" seconds");
                        txtCheckTime.setTextColor(Color.RED);
                        btnSendOtp.setVisibility(View.GONE);
                        counter++;
                    }
                    public  void onFinish(){
                        txtCheckTime.setVisibility(View.GONE);
                        btnSendOtp.setVisibility(View.VISIBLE);
                    }
                }.start();
            }
        });

        btnContinue= (Button) rootView.findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectOperator=txtSelectOperator.getText().toString();
                ConsumerIdBbps=edtConsumerIdBbps.getText().toString();
                CustomerMobileNoBbps=edtCustomerMobileNoBbps.getText().toString();
                BillingUnit=edtBillingUnit.getText().toString();

                if (checkConnection()) {
                    boolean isError=false;

                    if(null==selectOperator||selectOperator.length()==0||selectOperator.equalsIgnoreCase(""))
                    {
                        isError=true;
                        txtSelectOperator.setError("Select operator type");
                    }
                    if(null==ConsumerIdBbps||ConsumerIdBbps.length()==0||ConsumerIdBbps.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edtConsumerIdBbps.setError("Field Cannot be Blank");
                    }
                    if(null==CustomerMobileNoBbps||CustomerMobileNoBbps.length()==0||CustomerMobileNoBbps.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edtCustomerMobileNoBbps.setError("Field Cannot be Blank");
                    }
                    if(linearLayoutExtra.getVisibility()==View.VISIBLE) {
                        if (!isNotValidadminbillingunit(BillingUnit)) {
                            edtBillingUnit.setError("Field Cannot be Blank");
                            isError = true;
                        }
                    }else {
                        isError = false;
                    }
                   /* if(null==BillingUnit||BillingUnit.length()==0||BillingUnit.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edtBillingUnit.setError("Field Cannot be Blank");
                    }*/
                    if(!isError)
                    {
                        new GenerateBill().execute();
                    }
                }else
                {
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Internet Connection");
                }
            }
        });

        btnRegister= (Button) rootView.findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(mContext,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED

                        && ActivityCompat.checkSelfPermission(mContext,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

                } else {
                    //Toast.makeText(mContext, "You need have granted permission", Toast.LENGTH_SHORT).show();
                    gps = new GPSTracker(mContext, getActivity());

                    // Check if GPS enabled

                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();
/*
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();*/

                        Log.d("latlong",latitude+" "+longitude+"");
                        storeLatitude=Double.toString(latitude);
                        storeLongitude=Double.toString(longitude);

                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }
                }

                enterOTP=edtEnterOTP.getText().toString();
                panCard=edtPanCard.getText().toString();
                if (checkConnection()) {
                    boolean isError=false;
                    if(null==enterOTP||enterOTP.length()==0||enterOTP.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edtEnterOTP.setError("Enter the Valid OTP");
                    }
                    if(null==panCard||panCard.length()==0|| panCard.length() != 10|| isValidPanCard(panCard))
                    {
                        isError=true;
                        edtPanCard.setError("Enter Valid Pan Card No.");
                    }
                    if(!isError)
                    {
                        new RegisterRetailer().execute();
                    }

                }else
                {
                    txtRegisterError.setVisibility(View.VISIBLE);
                    txtRegisterError.setText("No Internet Connection");
                }

            }
        });

        return rootView;
    }

    private boolean isNotValidadminbillingunit(String BillingUnit) {
        if (BillingUnit == null || BillingUnit.length()>0) {
            return true;
        }
        return false;
    }

    public final static boolean isValidPanCard(String target) {

         /*   Pattern mPattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");
            Matcher mMatcher = mPattern.matcher(target);
            return mMatcher.matches();*/

        return Pattern.compile("[a-z]{5}[0-9]{4}[a-z]{1}").matcher(target).matches();

    }

    private class GenerateBill extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("operatorId", operator_id);
                parameters.put("requestOrigin", "app");
                parameters.put("consumerId", ConsumerIdBbps);
                parameters.put("customerNo", CustomerMobileNoBbps);
                parameters.put("option1", BillingUnit);
                this.response = new JSONObject(service.POST(DefineData.GENERATE_BILL,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("generateBill", response +"operator_id: "+operator_id+ " "+"ConsumerId: "+ConsumerIdBbps+ " "+"CustomerMobileNo: "+CustomerMobileNoBbps+" "+BillingUnit+" "+ DefineData.GENERATE_BILL);
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txtError.setText(msg+"");
                        txtError.setVisibility(View.VISIBLE);
                       /* linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);*/
                        loading.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);
                    } else {
                        JSONObject json = response.getJSONObject("data");
                        if(json.length()!=0) {
                            try {
                                String consumerId = json.getString("consumerId");
                                String billAmount = json.getString("billAmount");
                                String surcharge = json.getString("surcharge");
                                String totalAmount = json.getString("totalAmount");
                                String referenceId = json.getString("referenceId");

                                storeAmount = billAmount;
                                storeSurcharge = surcharge;
                                storeTotalAmount = totalAmount;
                                referenceID = referenceId;

                                JSONObject json2 = json.getJSONObject("billInfo");
                                String customername = json2.getString("customername");
                                storeCustomerName = customername;

                               /* JSONObject json2 = response.getJSONObject("billInfo");
                                String customername = json2.getString("customername");
                                Log.d("eere",customername+" ");*/

                                loading.setVisibility(View.GONE);
                                content.setVisibility(View.VISIBLE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                txtError.setText("Something went wrong! Try again");
                                /*linear_container.setVisibility(View.GONE);
                                rel_no_records.setVisibility(View.VISIBLE);*/
                                loading.setVisibility(View.GONE);
                                content.setVisibility(View.VISIBLE);
                            }
                            custdialog();
                        }else{
                            txtError.setText("Record not Found");
                            /*linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);*/
                            loading.setVisibility(View.GONE);
                            content.setVisibility(View.VISIBLE);
                        }
                    }
                }  catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("Something went wrong! Try again");
                    /* linear_container.setVisibility(View.VISIBLE);*/
                    loading.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);

                }
            }else{
                /*linear_container.setVisibility(View.VISIBLE);*/
                loading.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }
        }
    }

    private void custdialog(){
        final View dialogView = View.inflate(getActivity(),R.layout.confirm_generate_bill_layout,null);

        final Dialog dialog = new Dialog(getActivity());
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        TextView CustomerName = dialog.findViewById(R.id.txtCustomerName);
        TextView CustomerId = dialog.findViewById(R.id.CustomerId);
        TextView OperatorId= dialog.findViewById(R.id.OperatorId);
        TextView Amount= dialog.findViewById(R.id.Amount);
        TextView Surcharge= dialog.findViewById(R.id.Surcharge);
        TextView TotalAmount= dialog.findViewById(R.id.TotalAmount);
        Button btnPay = dialog.findViewById(R.id.btnPay);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        CustomerName.setText(storeCustomerName+"");
        CustomerId.setText(ConsumerIdBbps+"");
        OperatorId.setText(operator_name);
        Amount.setText("₹ "+storeAmount+"");
        Surcharge.setText(storeSurcharge);
        TotalAmount.setText("₹ "+storeTotalAmount);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new PayBill().execute();
            }
        });
        dialog.show();
    }

    private class PayBill extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("consumerId" , ConsumerIdBbps);
                parameters.put("option1", BillingUnit);
                parameters.put("operatorId", operator_id);
                parameters.put("customerNo", CustomerMobileNoBbps);
                parameters.put("requestOrigin", "app");
                parameters.put("amount",storeAmount);
                parameters.put("referenceId",referenceID);
                this.response = new JSONObject(service.POST(DefineData.PAY_BILL,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("payBill", response + " "+"customerId: :"+ConsumerIdBbps+" "+"billingUnit: "+BillingUnit+" "+"operator_id: " + operator_id+ " "+"mobileNo: " + CustomerMobileNoBbps + " "+"storeAmount: "+storeAmount+" "+"referenceId: "+referenceID+" "+DefineData.PAY_BILL);
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("data");
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(message+"");
                        loading.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);
                    }
                    else {
                        String msg=response.getString("data");
                        Intent i=new Intent(getActivity(), BbpsSuccessActivity.class);
                        i.putExtra("msg",msg);
                        i.putExtra("type","Bill Payment");
                        startActivity(i);
                        getActivity().finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("Something went wrong! Try again later");
                    loading.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                }
            }else{
                txtError.setVisibility(View.VISIBLE);
                txtError.setText("Empty response from server");
                loading.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }

        }
    }

    private class SendOTP extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            /* container.setVisibility(View.GONE);*/
            //loading.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.SEND_OTP,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            FirebaseCrash.log(response+"");

            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("data");
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(message+"");
                        backendError=message;
                    } else {
                        String msg=response.getString("data");
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText(backendError+"");

                }
            }else{
                txtError.setVisibility(View.VISIBLE);
                txtError.setText(backendError+"");
                /*  container.setVisibility(View.VISIBLE);*/
                //loading.setVisibility(View.GONE);
            }
        }
    }

    private class CheckStatus extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            /* container.setVisibility(View.GONE);*/
            //loading.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.CHECK_BBPS_STATUS,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            FirebaseCrash.log(response+"");

            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer bbpsVerified=response.getInt("bbpsVerified");

                    if (bbpsVerified==0)
                    {
                        checkStatus.setVisibility(View.VISIBLE);
                       /* msg = response.getString("responseDesc");
                        txtError.setText(msg);
                        txtError.setVisibility(View.VISIBLE);*/
                    } else
                    if (bbpsVerified==1)
                    {
                        linearLayout.setVisibility(View.VISIBLE);
                       /* msg = response.getString("responseDesc");
                        txtError.setText(msg);
                        txtError.setVisibility(View.VISIBLE);*/
                    }
                    else
                    {
                   /*     container.setVisibility(View.VISIBLE);
                        //loading.setVisibility(View.GONE);*/
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("Something went wrong! Try Again");
                    /* container.setVisibility(View.VISIBLE);*/
                    //loading.setVisibility(View.GONE);
                }
            }else{
                txtError.setVisibility(View.VISIBLE);
                txtError.setText("Something went wrong! Try Again");
                /*  container.setVisibility(View.VISIBLE);*/
                //loading.setVisibility(View.GONE);
            }
        }
    }

    private class RegisterRetailer extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("otp", enterOTP);
                parameters.put("pan_card", panCard);
                parameters.put("lat", storeLatitude);
                parameters.put("long", storeLongitude);

                this.response = new JSONObject(service.POST(DefineData.REGISTER_RETAILER,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("dfrg", response + " "+enterOTP+" "+panCard+" "+storeLatitude+" " + storeLongitude+ " " +DefineData.SUBMIT_DETAILS);
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("data");
                        txtRegisterError.setVisibility(View.VISIBLE);
                        txtRegisterError.setText(message+"");
                        registerError=message;
                        loading.setVisibility(View.GONE);
                    } else {
                        String msg=response.getString("data");
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        Intent i=new Intent(getActivity(), BillActivity.class);
                        getActivity().startActivity(i);
                        getActivity().finish();
                        loading.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtRegisterError.setVisibility(View.VISIBLE);
                    txtRegisterError.setText(registerError+"");
                    loading.setVisibility(View.GONE);
                }
            }else{
                loading.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case 5:
                if (null!=data) {
                    operator_id = data.getStringExtra("operator_id");
                    operator_name = data.getStringExtra("operator_name");
                    Log.d("ryet", operator_name + " ");
                    txtSelectOperator.setText(operator_name + "");
                    //new checkStatus().execute();
                    switch (operator_name) {
                        case "Tata Power - Mumbai":
                            linearLayoutExtra.setVisibility(View.GONE);
                            edtBillingUnit.setText("");
                            new CheckStatus().execute();
                            break;
                        case "SNDL Power - Nagpur":
                            linearLayoutExtra.setVisibility(View.GONE);
                            edtBillingUnit.setText("");
                            new CheckStatus().execute();
                            break;
                        case "Best Undertaking - Mumbai":
                            linearLayoutExtra.setVisibility(View.GONE);
                            edtBillingUnit.setText("");
                            new CheckStatus().execute();
                            break;
                        case "Torrent Power  - Mumbai":
                            linearLayoutExtra.setVisibility(View.VISIBLE);
                            textViewConsumerId.setText("Service no :");
                            textViewBillingUnit.setText("City :");
                            new CheckStatus().execute();
                            break;
                        case "MSEDC - Maharashtra":
                            linearLayoutExtra.setVisibility(View.VISIBLE);
                            textViewConsumerId.setText("Consumer id :");
                            textViewBillingUnit.setText("Billing unit (BU):");
                            new CheckStatus().execute();
                            break;
                        case "Adani Power":
                            linearLayoutExtra.setVisibility(View.GONE);
                            edtBillingUnit.setText("");
                            new CheckStatus().execute();
                            break;
                        case "WBSEDCL - WEST BENGAL":
                            linearLayoutExtra.setVisibility(View.GONE);
                            edtBillingUnit.setText("");
                            new CheckStatus().execute();
                            break;
                    }
                }
                break;
        }
    }

    @Override    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0

                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    gps = new GPSTracker(mContext, getActivity());

                    // Check if GPS enabled

                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();

                       /* Toast.makeText(getActivity().getApplicationContext(),
                                "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();*/
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(mContext, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}