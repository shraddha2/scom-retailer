package com.scom5g.retailer.LastTenTransaction;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scom5g.retailer.AEPS.AepsAdapter;
import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.DividerItemDecoration;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;
import com.scom5g.retailer.Scom5GRetailer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AepsLastTransactionFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private AepsAdapter mAdapter;
    String token;
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg;
    ConstraintLayout progress_linear,linear_container,rel_no_records,rel_no_internet;
    SwipeRefreshLayout mSwipeRefreshLayout;

    public AepsLastTransactionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.activity_aeps_last_transaction_fragment, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Scom5GRetailer.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        mSwipeRefreshLayout= (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_recharge_history);
        //txt_title= (TextView) rootView.findViewById(R.id.txt_title);

        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        //txt_title.setText("Latest Transactions");

        mAdapter = new AepsAdapter(movieList,getActivity(),"Last 10 transactions");
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchLatestAepsTransaction().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        return rootView;
    }

    void refreshItems() {
        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchLatestAepsTransaction().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private class FetchLatestAepsTransaction extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.AEPS_LAST_TEN_TRANSACTION,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            //Log.d("dfrg", response + " "+mobile_no+" "+pass_word+" "+DefineData.BILL_PAYMENT_LAST_TRANSACTION);

            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("message")) {
                            msg = response.getString("message");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = response.getJSONArray("data");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    //Getting json
                                    json2 = jsonArray.getJSONObject(i);

                                    String transactionId = json2.getString("transactionId");
                                    String amount = json2.getString("amount");
                                    String retailer_commission = json2.getString("retailer_commission");
                                    String updatedTime = json2.getString("updatedTime");
                                    String bankTransactionId = json2.getString("bankTransactionId");
                                    String transactionType = json2.getString("transactionType");
                                    String remark = json2.getString("remark");
                                    String time = json2.getString("time");
                                    String status = json2.getString("status");
                                    superHero = new Item(transactionId, "₹ " + amount, retailer_commission,updatedTime,bankTransactionId,transactionType,remark,time,status);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setText("Error in parsing");
                                    linear_container.setVisibility(View.GONE);
                                    rel_no_records.setVisibility(View.VISIBLE);
                                    progress_linear.setVisibility(View.GONE);
                                }
                                movieList.add(superHero);
                            }
                            mAdapter.notifyDataSetChanged();
                            linear_container.setVisibility(View.VISIBLE);
                            rel_no_records.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }else{
                            txt_err_msgg.setText("Records Not Found");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Error in parsing");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_err_msgg.setText("Empty server response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        Scom5GRetailer.getInstance().setConnectivityListener(this);

    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
    }
}