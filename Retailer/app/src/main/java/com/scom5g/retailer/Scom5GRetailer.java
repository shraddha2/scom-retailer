package com.scom5g.retailer;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;


public class Scom5GRetailer extends Application {

private static Scom5GRetailer mInstance;
        private FirebaseAnalytics mFirebaseAnalytics;
@Override
public void onCreate() {
        super.onCreate();

        mInstance = this;
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }

public static synchronized Scom5GRetailer getInstance() {
        return mInstance;

        }

public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
        }


        }
