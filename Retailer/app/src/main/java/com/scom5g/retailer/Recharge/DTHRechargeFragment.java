package com.scom5g.retailer.Recharge;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.NavigationDrawer.HomeActivity;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;
import com.scom5g.retailer.Scom5GRetailer;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class DTHRechargeFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    TextView txt_title,txt_label1,txt_label2,txt_label3,txt_label5,txt_label6,txt_label7;
    EditText edt_cust_id,edt_amt;
    Button btn_recharge,customer_info,dth_plan;
    String amt,cust_number;
    String token="",operator_id,operator_name,fetch_amt,fetch_amt2,fetch_amt3,fetch_amt4;
    ImageView img_logo;
    SharedPreferences sharedpreferences;
    TextView txt_error;
    LinearLayout progress_linear,linear_container;
    TextView edt_operator_circle;
    String operator_circle_id="8",operator_circle_name="Maharashtra/Goa";
    SharedPreferences.Editor editor;
    int drawable_icon;
    // Typeface font ;
    public DTHRechargeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_dthrecharge, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);
        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Scom5GRetailer.getInstance().MODE_PRIVATE);
        editor = sharedpreferences.edit();
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        img_logo= (ImageView) rootView.findViewById(R.id.img_logo);
        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2= (TextView) rootView.findViewById(R.id.txt_label2);
        txt_label3= (TextView) rootView.findViewById(R.id.txt_label3);
        txt_label5= (TextView) rootView.findViewById(R.id.txt_label5);
        txt_label6= (TextView) rootView.findViewById(R.id.txt_label6);
        txt_label7= (TextView) rootView.findViewById(R.id.txt_label7);
        customer_info= (Button) rootView.findViewById(R.id.customer_info);
        dth_plan= (Button) rootView.findViewById(R.id.dth_plan);
        btn_recharge= (Button) rootView.findViewById(R.id.btn_recharge);
        edt_cust_id= (EditText) rootView.findViewById(R.id.edt_cust_id);
        edt_amt= (EditText) rootView.findViewById(R.id.edt_amt);
        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        edt_operator_circle= (TextView) rootView.findViewById(R.id.edt_operator_circle);

        progress_linear.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);


        Bundle bundle = getArguments();
        operator_name = bundle.getString("operator_name");
        operator_id = bundle.getString("operator_id");

        /*if(operator_id.equalsIgnoreCase("19"))
        {
            edt_cust_id.setFilters(new InputFilter[] { new InputFilter.LengthFilter(10) });

        }
        if(operator_id.equalsIgnoreCase("20"))
        {
            edt_cust_id.setFilters(new InputFilter[] { new InputFilter.LengthFilter(10) });

        }
        if(operator_id.equalsIgnoreCase("21"))
        {
            edt_cust_id.setFilters(new InputFilter[] { new InputFilter.LengthFilter(11) });

        }
        if(operator_id.equalsIgnoreCase("22"))
        {
            edt_cust_id.setFilters(new InputFilter[] { new InputFilter.LengthFilter(12) });

        }
        if(operator_id.equalsIgnoreCase("23"))
        {
            edt_cust_id.setFilters(new InputFilter[] { new InputFilter.LengthFilter(12) });

        }
        if(operator_id.equalsIgnoreCase("24"))
        {
            edt_cust_id.setFilters(new InputFilter[] { new InputFilter.LengthFilter(11) });

        }*/

        txt_label2.setText(operator_name + "");
        operator_circle_name=sharedpreferences.getString(DefineData.KEY_OPERATOR_CIRCLE_DTH,"Maharashtra/Goa");
        operator_circle_id=sharedpreferences.getString(DefineData.KEY_OPERATOR_CIRCLE_ID_DTH,"8");

        edt_operator_circle.setText(operator_circle_name + "");
        ChangeIcon(operator_id);

        customer_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cust_number = edt_cust_id.getText().toString();
                if (checkConnection()) {
                    boolean isError = false;
                    if (null == cust_number || cust_number.length() == 0 || cust_number.equalsIgnoreCase("")) {
                        isError = true;
                        edt_cust_id.setError("Enter VC Number");
                    }
                    if (!isError) {
                        Intent i=new Intent(getActivity(), CustomerInfoActivity.class);
                        i.putExtra("operator_id",operator_id);
                        i.putExtra("cust_number",cust_number);
                        startActivity(i);
                    }
                } else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setTextColor(ContextCompat.getColor(getActivity(), R.color.status_fail));
                    txt_error.setText("No Internet Connection");
                }
            }
        });

        dth_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cust_number= edt_cust_id.getText().toString();
                if (checkConnection()) {
                    boolean isError = false;
                    if (null == cust_number || cust_number.length() == 0 || cust_number.equalsIgnoreCase("")) {
                        isError = true;
                        edt_cust_id.setError("Enter VC Number");
                    }
                    if (!isError) {

                        Intent i=new Intent(getActivity(), DthPlanActivity.class);
                        i.putExtra("operator_id",operator_id);
                        i.putExtra("cust_number",cust_number);
                        startActivityForResult(i, 22);
                    }
                } else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setTextColor(ContextCompat.getColor(getActivity(), R.color.status_fail));
                    txt_error.setText("No Internet Connection");
                }
            }
        });

        txt_label3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),OperatorCircleActivity.class);
                intent.putExtra("source","rech");
                startActivityForResult(intent, 3);

            }
        });
        btn_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amt= edt_amt.getText().toString();
                cust_number= edt_cust_id.getText().toString();
                if (checkConnection()) {
                    boolean isError=false;
                    if(null==amt||amt.length()==0||amt.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_amt.setError("Field Cannot be Blank");
                    }
                    if(null==cust_number||cust_number.length()==0||cust_number.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_cust_id.setError("Invalid Mobile Number");
                    }
                    if(!isError)
                    {
                        custdialog();
                    }
                }else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setTextColor(ContextCompat.getColor(getActivity(),R.color.status_fail));
                    txt_error.setText("No Internet Connection");
                }
            }
        });
        return rootView;
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case 2:
                if (null!=data) {
                    operator_id = data.getStringExtra("operator_id");
                    operator_name = data.getStringExtra("operator_name");
                    txt_label2.setText(operator_name + "");
                    ChangeIcon(operator_id);
                }
                break;

            case 3:
                if (null!=data) {
                    operator_circle_id = data.getStringExtra("operator_circle_id");
                    operator_circle_name = data.getStringExtra("operator_circle_name");
                    edt_operator_circle.setText(operator_circle_name + "");

                }else{

                    Bundle bundle=new Bundle();
                    bundle.putString("operator_name", operator_name);
                    bundle.putString("operator_id", operator_id);
                    android.app.Fragment frg = new DTHRechargeFragment();
                    frg.setArguments(bundle);
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"DTH")
                            .addToBackStack(null)
                            .commit();
                }
                break;

            case 22:
                if (null!=data) {
                    fetch_amt = data.getStringExtra("amount");
                    edt_amt.setText(fetch_amt + "");
                }
                break;
        }
    }

    private class RechargeDTH extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("recharge_no", cust_number);
                parameters.put("recharge_operator", operator_id);
                parameters.put("recharge_amount", amt);
                parameters.put("request_origin", "app");
                //parameters.put("recharge_circle", operator_circle_id);
                this.response = new JSONObject(service.POST(DefineData.RECHARGE_URL,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("data");
                        txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText(message+"");
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        String msg=response.getString("data");
                        Intent i=new Intent(getActivity(), SuccessActivity.class);
                        i.putExtra("msg",msg);
                        i.putExtra("type","DTH Recharge");
                        startActivity(i);
                        getActivity().finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_error.setVisibility(View.VISIBLE);
                txt_error.setText("Empty server response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }
    private void ChangeIcon(String id)
    {
        switch(id)
        {
            case "1":
                drawable_icon=R.drawable.ic_airtel;
                Picasso.with(getActivity()).load(R.drawable.ic_airtel).fit().into(img_logo);
                break;
            case "2":
                drawable_icon=R.drawable.ic_aircel;
                Picasso.with(getActivity()).load(R.drawable.ic_aircel).fit().into(img_logo);
                break;
            case "3":
                drawable_icon=R.drawable.ic_idea_logo;
                Picasso.with(getActivity()).load(R.drawable.ic_idea_logo).fit().into(img_logo);
                break;
            case "4":
                drawable_icon=R.drawable.ic_vodafone;
                Picasso.with(getActivity()).load(R.drawable.ic_vodafone).fit().into(img_logo);
                break;
            case "5":
                drawable_icon=R.drawable.ic_bsnl;
                Picasso.with(getActivity()).load(R.drawable.ic_bsnl).fit().into(img_logo);
                break;
            case "6":
                drawable_icon=R.drawable.ic_bsnl;
                Picasso.with(getActivity()).load(R.drawable.ic_bsnl).fit().into(img_logo);
                break;
            case "7":
                drawable_icon=R.drawable.ic_telenor;
                Picasso.with(getActivity()).load(R.drawable.ic_telenor).fit().into(img_logo);
                break;
            case "8":
                drawable_icon=R.drawable.ic_telenor;
                Picasso.with(getActivity()).load(R.drawable.ic_telenor).fit().into(img_logo);
                break;
            case "9":
                drawable_icon=R.drawable.ic_jio;
                Picasso.with(getActivity()).load(R.drawable.ic_jio).fit().into(img_logo);
                break;
            case "10":
                drawable_icon=R.drawable.ic_docomo;
                Picasso.with(getActivity()).load(R.drawable.ic_docomo).fit().into(img_logo);
                break;
            case "11":
                drawable_icon=R.drawable.ic_docomo;
                Picasso.with(getActivity()).load(R.drawable.ic_docomo).fit().into(img_logo);
                break;
            case "12":
                drawable_icon=R.drawable.ic_mtnl;
                Picasso.with(getActivity()).load(R.drawable.ic_mtnl).fit().into(img_logo);
                break;
            case "13":
                drawable_icon=R.drawable.ic_mtnl;
                Picasso.with(getActivity()).load(R.drawable.ic_mtnl).fit().into(img_logo);
                break;
            case "14":
                drawable_icon=R.drawable.ic_airtel;
                Picasso.with(getActivity()).load(R.drawable.ic_airtel).fit().into(img_logo);
                break;
            case "15":
                drawable_icon=R.drawable.ic_idea_logo;
                Picasso.with(getActivity()).load(R.drawable.ic_idea_logo).fit().into(img_logo);
                break;
            case "16":
                drawable_icon=R.drawable.ic_vodafone;
                Picasso.with(getActivity()).load(R.drawable.ic_vodafone).fit().into(img_logo);
                break;
            case "17":
                drawable_icon=R.drawable.ic_aircel;
                Picasso.with(getActivity()).load(R.drawable.ic_aircel).fit().into(img_logo);
                break;
            case "18":
                drawable_icon=R.drawable.ic_docomo;
                Picasso.with(getActivity()).load(R.drawable.ic_docomo).fit().into(img_logo);
                break;
            case "19":
                drawable_icon=R.drawable.ic_airtel_tv;
                Picasso.with(getActivity()).load(R.drawable.ic_airtel_tv).fit().into(img_logo);
                break;
            case "20":
                drawable_icon=R.drawable.ic_tata_sky;
                Picasso.with(getActivity()).load(R.drawable.ic_tata_sky).fit().into(img_logo);
                break;
            case "21":
                drawable_icon=R.drawable.ic_dish_tv;
                Picasso.with(getActivity()).load(R.drawable.ic_dish_tv).fit().into(img_logo);
                break;
            case "22":
                drawable_icon=R.drawable.ic_videocon;
                Picasso.with(getActivity()).load(R.drawable.ic_videocon).fit().into(img_logo);
                break;
            case "23":
                drawable_icon=R.drawable.ic_big_tv;
                Picasso.with(getActivity()).load(R.drawable.ic_big_tv).fit().into(img_logo);
                break;
            case "24":
                drawable_icon=R.drawable.ic_sun_direct;
                Picasso.with(getActivity()).load(R.drawable.ic_sun_direct).fit().into(img_logo);
                break;
            default:   drawable_icon=R.drawable.ic_airtel;
                Picasso.with(getActivity()).load(R.drawable.ic_airtel).fit().into(img_logo);
        }
    }
    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {

        super.onResume();
        txt_label3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    txt_label3.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_operator_circle.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_operator_circle.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_cust_id.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_cust_id.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        edt_amt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_amt.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        Scom5GRetailer.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    Fragment frg=new DTHOperatorFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("trans_type", "DTH Recharge");
                    frg.setArguments(bundle);
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"DTH Recharge")
                            .addToBackStack(null)
                            .commit();
                    return true;

                }

                return false;
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private void custdialog(){
        final View dialogView = View.inflate(getActivity(),R.layout.confirm_dialog,null);

        final Dialog dialog = new Dialog(getActivity(),R.style.FullScreenDialogStyle);
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        ImageView img_logos=(ImageView)dialog.findViewById(R.id.img_logos);
        TextView txt_amt=(TextView)dialog.findViewById(R.id.txt_amt);
        TextView txt_operator_circle=(TextView)dialog.findViewById(R.id.txt_operator_circle);
        TextView txt_rec_number=(TextView)dialog.findViewById(R.id.txt_rec_number);
        TextView txt_label=(TextView)dialog.findViewById(R.id.txt_label);
        Button btn_confirm = (Button) dialog.findViewById(R.id.btn_confirm);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        TextView txt_op_name=(TextView)dialog.findViewById(R.id.txt_op_name);
        txt_op_name.setText(operator_name);
        txt_label.setText("Customer Id");
        Picasso.with(Scom5GRetailer.getInstance()).load(drawable_icon).fit().into(img_logos);
        txt_amt.setText("₹ "+amt);
        txt_operator_circle.setText(operator_circle_name);
        txt_rec_number.setText(cust_number);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new RechargeDTH().execute();
            }
        });
        dialog.show();
    }

}
