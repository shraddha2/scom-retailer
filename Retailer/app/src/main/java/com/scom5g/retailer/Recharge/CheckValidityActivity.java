package com.scom5g.retailer.Recharge;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.NavigationDrawer.HomeActivity;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class CheckValidityActivity extends AppCompatActivity {

    ImageView img_profile_pic;
    // Typeface font ;
    TextView txtOperator,txtMobileNo,txtBalanceType,txtExpiryDate,txtBalance;
    String token;
    SharedPreferences sharedpreferences;
    RelativeLayout rel_no_records,rel_no_internet;
    TextView txt_err_msgg,txt_network_msg;
    LinearLayout progress_linear,linear_container;
    ImageView img_edit;
    Context ctx;
    String operator_id="", mobile_number="", operator_name="";
    Button backButton;

    public CheckValidityActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_validity);

        //img_profile_pic= (ImageView) getActivity().findViewById(R.id.img_profile_pic);
        txtOperator = (TextView) findViewById(R.id.txtOperator);
        txtMobileNo = (TextView) findViewById(R.id.txtMobileNo);
        txtBalanceType = (TextView) findViewById(R.id.txtBalanceType);
        txtExpiryDate = (TextView) findViewById(R.id.txtExpiryDate);
        txtBalance = (TextView) findViewById(R.id.txtBalance);

        txt_err_msgg= (TextView) findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) findViewById(R.id.txt_network_msg);
        progress_linear= (LinearLayout) findViewById(R.id.loding);
        linear_container= (LinearLayout) findViewById(R.id.container);
        rel_no_records= (RelativeLayout) findViewById(R.id.rel_no_records);
        rel_no_internet= (RelativeLayout) findViewById(R.id.rel_no_internet);
        backButton= (Button) findViewById(R.id.backButton);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        operator_id=getIntent().getStringExtra("operator_id");
        mobile_number=getIntent().getStringExtra("mobile_number");
        operator_name=getIntent().getStringExtra("operator_name");
        Log.d("bundle123456",operator_id+" "+mobile_number+" "+operator_name+" ");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchCustomerDetails().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

    }

    private class FetchCustomerDetails extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("operatorId", operator_id);
                parameters.put("phone",mobile_number);
                this.response = new JSONObject(service.POST(DefineData.CHECK_VALIDITY,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("res789654",operator_id+ " "+mobile_number+" "+response+ "");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        JSONObject json = response.getJSONObject("records");
                        JSONArray message=json.getJSONArray("Message");
                        //String message = json.getString("Message");
                        //String message = response.getString("records");
                        txt_err_msgg.setVisibility(View.VISIBLE);
                        txt_err_msgg.setText(message+"");
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONObject json = response.getJSONObject("data");
                        JSONObject json2=json.getJSONObject("records");
                        if(json.length()!=0) {
                            try {
                                String Balancetype = json2.getString("Balancetype");
                                String ExpiryDate = json2.getString("ExpiryDate");
                                String Balance = json2.getString("Balance");
                                txtOperator.setText(operator_name+"");
                                txtMobileNo.setText(mobile_number+"");
                                txtBalanceType.setText(Balancetype+"");
                                txtExpiryDate.setText(ExpiryDate+"");
                                txtBalance.setText(Balance+"");
                                linear_container.setVisibility(View.VISIBLE);
                                progress_linear.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                linear_container.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("No Records Found!");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);

                }
            }
            else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent();
        setResult(5,intent);
        finish();//finishing activity

    }
}
