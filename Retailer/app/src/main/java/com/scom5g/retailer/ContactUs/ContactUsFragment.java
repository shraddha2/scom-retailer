package com.scom5g.retailer.ContactUs;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scom5g.retailer.HomePage.HomeFragment;
import com.scom5g.retailer.R;

public class ContactUsFragment extends Fragment {

    TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_about,txt_addr,txt_email,txt_contact,txt_title;
    // Typeface font ;
    public ContactUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_contact_us, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);

        txt_label1 = (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2 = (TextView) rootView.findViewById(R.id.txt_label2);
        txt_label3 = (TextView) rootView.findViewById(R.id.txt_label3);
        txt_label4 = (TextView) rootView.findViewById(R.id.txt_label4);

        txt_about =  (TextView) rootView.findViewById(R.id.txt_about);
        txt_addr =   (TextView) rootView.findViewById(R.id.txt_addr);
        txt_email =  (TextView) rootView.findViewById(R.id.txt_email);
        txt_contact = (TextView) rootView.findViewById(R.id.txt_contact);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);

        return rootView;
    }

    @Override
    public void onResume() {

        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }
}
