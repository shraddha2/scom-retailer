package com.scom5g.retailer.BillPayment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.scom5g.retailer.DefineData;
import com.scom5g.retailer.Model.HTTPURLConnection;
import com.scom5g.retailer.NavigationDrawer.HomeActivity;
import com.scom5g.retailer.R;
import com.scom5g.retailer.Receiver.ConnectivityReceiver;
import com.scom5g.retailer.Scom5GRetailer;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.regex.Pattern;

public class BroadBandFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener{

    TextView txtServiceProvider,textView19,txtError,txtCheckTime,txtRegisterError;
    EditText edtCustomerId,edtMobileNumber,edtEnterOTP,edtPanCard;
    Button btnContinue,btnSendOtp,btnRegister;
    String customerId,enterOTP, panCard, mobileNo,operator_id="",operator_name="",token,backendError,registerError,storeLatitude,storeLongitude;
    SharedPreferences sharedpreferences;
    public int counter;
    ConstraintLayout linearLayout,checkStatus,loading,content;
    GPSTracker gps;
    Context mContext;
    String storeAmount,storeSurcharge,storeTotalAmount, referenceID, billingUnit=" ",serviceProvider, storeCustomerName;

    public BroadBandFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_broad_band, container, false);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Scom5GRetailer.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        mContext = getActivity();

        content = (ConstraintLayout) rootView.findViewById(R.id.container);
        textView19= (TextView) rootView.findViewById(R.id.textView19);
        edtCustomerId= (EditText) rootView.findViewById(R.id.edtCustomerId);
        edtMobileNumber= (EditText) rootView.findViewById(R.id.edtMobileNumber);
        edtEnterOTP= (EditText) rootView.findViewById(R.id.edtEnterOTP);
        edtPanCard= (EditText) rootView.findViewById(R.id.edtPanCard);
        txtError= (TextView) rootView.findViewById(R.id.txtError);
        txtRegisterError= (TextView) rootView.findViewById(R.id.txtRegisterError);
        linearLayout= (ConstraintLayout) rootView.findViewById(R.id.linearLayout);
        checkStatus= (ConstraintLayout) rootView.findViewById(R.id.checkStatus);
        checkStatus.setVisibility(View.GONE);

        loading= (ConstraintLayout) rootView.findViewById(R.id.loading);

        txtServiceProvider= (TextView) rootView.findViewById(R.id.txtServiceProvider);
        txtServiceProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),BroadBandOperatorListActivity.class);
                startActivityForResult(intent, 5);
            }
        });

        txtCheckTime= (TextView) rootView.findViewById(R.id.txtCheckTime);
        txtCheckTime.setVisibility(View.GONE);
        btnSendOtp= (Button) rootView.findViewById(R.id.btnSendOtp);
        btnSendOtp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                new SendOTP().execute();
                new CountDownTimer(30000, 1000){
                    public void onTick(long millisUntilFinished){
                        txtCheckTime.setVisibility(View.VISIBLE);
                        txtCheckTime.setText("Resend OTP after "+String.valueOf(counter)+" seconds");
                        txtCheckTime.setTextColor(Color.RED);
                        btnSendOtp.setVisibility(View.GONE);
                        counter++;
                    }
                    public  void onFinish(){
                        txtCheckTime.setVisibility(View.GONE);
                        btnSendOtp.setVisibility(View.VISIBLE);
                    }
                }.start();
            }
        });

        btnContinue= (Button) rootView.findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                serviceProvider=txtServiceProvider.getText().toString();
                customerId=edtCustomerId.getText().toString();
                mobileNo=edtMobileNumber.getText().toString();

                if (checkConnection()) {
                    boolean isError=false;

                    if(null==serviceProvider||serviceProvider.length()==0||serviceProvider.equalsIgnoreCase(""))
                    {
                        isError=true;
                        txtServiceProvider.setError("Select operator type");
                    }
                    if(null==customerId||customerId.length()==0||customerId.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edtCustomerId.setError("Field Cannot be Blank");
                    }
                    if(null==mobileNo||mobileNo.length()==0||mobileNo.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edtMobileNumber.setError("Field Cannot be Blank");
                    }
                    if(!isError)
                    {
                        new GenerateBill().execute();
                    }
                }else
                {
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Internet Connection");
                }
            }
        });

        btnRegister= (Button) rootView.findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(mContext,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED

                        && ActivityCompat.checkSelfPermission(mContext,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

                } else {
                    //Toast.makeText(mContext, "You need have granted permission", Toast.LENGTH_SHORT).show();
                    gps = new GPSTracker(mContext, getActivity());

                    // Check if GPS enabled

                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();
/*
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();*/

                        Log.d("latlong",latitude+" "+longitude+"");
                        storeLatitude=Double.toString(latitude);
                        storeLongitude=Double.toString(longitude);

                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }
                }

                enterOTP=edtEnterOTP.getText().toString();
                panCard=edtPanCard.getText().toString();
                if (checkConnection()) {
                    boolean isError=false;
                    if(null==enterOTP||enterOTP.length()==0||enterOTP.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edtEnterOTP.setError("Enter the Valid OTP");
                    }
                    if(null==panCard||panCard.length()==0|| panCard.length() != 10|| isValidPanCard(panCard))
                    {
                        isError=true;
                        edtPanCard.setError("Enter Valid Pan Card No.");
                    }
                    if(!isError)
                    {
                        new RegisterRetailer().execute();
                    }

                }else
                {
                    txtRegisterError.setVisibility(View.VISIBLE);
                    txtRegisterError.setText("No Internet Connection");
                }

            }
        });

        return rootView;
    }

    public final static boolean isValidPanCard(String target) {

         /*   Pattern mPattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");
            Matcher mMatcher = mPattern.matcher(target);
            return mMatcher.matches();*/

        return Pattern.compile("[a-z]{5}[0-9]{4}[a-z]{1}").matcher(target).matches();

    }

    private class GenerateBill extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("operatorId", operator_id);
                parameters.put("requestOrigin", "app");
                parameters.put("consumerId", customerId);
                parameters.put("customerNo", mobileNo);
                this.response = new JSONObject(service.POST(DefineData.GENERATE_BILL,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("generateBill", response +"operator_id: "+operator_id+ " "+"ConsumerId: "+customerId+ " "+"CustomerMobileNo: "+mobileNo+" "+ DefineData.GENERATE_BILL);
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txtError.setText(msg+"");
                        txtError.setVisibility(View.VISIBLE);
                       /* linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);*/
                        loading.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);
                    } else {
                        JSONObject json = response.getJSONObject("data");
                        if(json.length()!=0) {

                            try {
                                String consumerId = json.getString("consumerId");
                                String billAmount = json.getString("billAmount");
                                String surcharge = json.getString("surcharge");
                                String totalAmount = json.getString("totalAmount");
                                String referenceId = json.getString("referenceId");

                                storeAmount = billAmount;
                                storeSurcharge = surcharge;
                                storeTotalAmount = totalAmount;
                                referenceID = referenceId;

                                JSONObject json2 = json.getJSONObject("billInfo");
                                String customername = json2.getString("customername");
                                storeCustomerName = customername;

                                loading.setVisibility(View.GONE);
                                content.setVisibility(View.VISIBLE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                txtError.setText("Something went wrong! Try again");
                                /*linear_container.setVisibility(View.GONE);
                                rel_no_records.setVisibility(View.VISIBLE);*/
                                loading.setVisibility(View.GONE);
                                content.setVisibility(View.VISIBLE);
                            }
                            custdialog();
                        }else{
                            txtError.setText("Record not Found");
                            /*linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);*/
                            loading.setVisibility(View.GONE);
                            content.setVisibility(View.VISIBLE);
                        }
                    }
                }  catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("Something went wrong! Try again");
                    /* linear_container.setVisibility(View.VISIBLE);*/
                    loading.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);

                }
            }else{
                /*linear_container.setVisibility(View.VISIBLE);*/
                loading.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }
        }
    }

    private void custdialog(){
        final View dialogView = View.inflate(getActivity(),R.layout.confirm_generate_bill_layout,null);

        final Dialog dialog = new Dialog(getActivity());
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        TextView CustomerName = dialog.findViewById(R.id.txtCustomerName);
        TextView CustomerId = dialog.findViewById(R.id.CustomerId);
        TextView OperatorId= dialog.findViewById(R.id.OperatorId);
        TextView Amount= dialog.findViewById(R.id.Amount);
        TextView Surcharge= dialog.findViewById(R.id.Surcharge);
        TextView TotalAmount= dialog.findViewById(R.id.TotalAmount);
        Button btnPay = dialog.findViewById(R.id.btnPay);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        CustomerName.setText(storeCustomerName+"");
        CustomerId.setText(customerId+"");
        OperatorId.setText(operator_name);
        Amount.setText("₹ "+storeAmount+"");
        Surcharge.setText(storeSurcharge);
        TotalAmount.setText("₹ "+storeTotalAmount);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new PayBill().execute();
            }
        });
        dialog.show();
    }

    private class PayBill extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("consumerId" , customerId);
                parameters.put("option1", billingUnit);
                parameters.put("operatorId", operator_id);
                parameters.put("customerNo", mobileNo);
                parameters.put("requestOrigin", "app");
                parameters.put("amount",storeAmount);
                parameters.put("referenceId",referenceID);
                this.response = new JSONObject(service.POST(DefineData.PAY_BILL,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("payBill", response + " "+"customerId: :"+customerId+" "+"billingUnit: "+billingUnit+" "+"operator_id: " + operator_id+ " "+"mobileNo: " + mobileNo + " "+"storeAmount: "+storeAmount+" "+"referenceId: "+referenceID+" "+DefineData.PAY_BILL);
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("data");
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(message+"");
                        loading.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);
                    }
                    else {
                        String msg=response.getString("data");
                        Intent i=new Intent(getActivity(), BbpsSuccessActivity.class);
                        i.putExtra("msg",msg);
                        i.putExtra("type","Bill Payment");
                        startActivity(i);
                        getActivity().finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("Error in parsing response");
                    loading.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                }
            }else{
                txtError.setVisibility(View.VISIBLE);
                txtError.setText("Empty response from server");
                loading.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }

        }
    }


    private class SendOTP extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            /* container.setVisibility(View.GONE);*/
            //loading.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.SEND_OTP,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            FirebaseCrash.log(response+"");

            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("data");
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(message+"");
                        backendError=message;
                    } else {
                        String msg=response.getString("data");
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText(backendError+"");

                }
            }else{
                txtError.setVisibility(View.VISIBLE);
                txtError.setText(backendError+"");
                /*  container.setVisibility(View.VISIBLE);*/
                //loading.setVisibility(View.GONE);
            }
        }
    }

    private class CheckStatus extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            /* container.setVisibility(View.GONE);*/
            //loading.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.CHECK_BBPS_STATUS,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            FirebaseCrash.log(response+"");

            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer bbpsVerified=response.getInt("bbpsVerified");

                    if (bbpsVerified==0)
                    {
                        checkStatus.setVisibility(View.VISIBLE);
                       /* msg = response.getString("responseDesc");
                        txtError.setText(msg);
                        txtError.setVisibility(View.VISIBLE);*/
                    } else
                    if (bbpsVerified==1)
                    {
                        linearLayout.setVisibility(View.VISIBLE);
                       /* msg = response.getString("responseDesc");
                        txtError.setText(msg);
                        txtError.setVisibility(View.VISIBLE);*/
                    }
                    else
                    {
                   /*     container.setVisibility(View.VISIBLE);
                        //loading.setVisibility(View.GONE);*/
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("Something went wrong! Try Again");
                    /* container.setVisibility(View.VISIBLE);*/
                    //loading.setVisibility(View.GONE);
                }
            }else{
                txtError.setVisibility(View.VISIBLE);
                txtError.setText("Something went wrong! Try Again");
                /*  container.setVisibility(View.VISIBLE);*/
                //loading.setVisibility(View.GONE);
            }
        }
    }

    private class RegisterRetailer extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("otp", enterOTP);
                parameters.put("pan_card", panCard);
                parameters.put("lat", storeLatitude);
                parameters.put("long", storeLongitude);

                this.response = new JSONObject(service.POST(DefineData.REGISTER_RETAILER,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("dfrg", response + " "+enterOTP+" "+storeLatitude+" " + storeLongitude+ " " +DefineData.SUBMIT_DETAILS);
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("data");
                        txtRegisterError.setVisibility(View.VISIBLE);
                        txtRegisterError.setText(message+"");
                        registerError=message;

                    } else {
                        String msg=response.getString("data");
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        Intent i=new Intent(getActivity(), BillActivity.class);
                        getActivity().startActivity(i);
                        getActivity().finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtRegisterError.setVisibility(View.VISIBLE);
                    txtRegisterError.setText(registerError+"");

                }
            }else{

            }

        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case 5:
                if (null!=data) {
                    operator_id = data.getStringExtra("operator_id");
                    operator_name = data.getStringExtra("operator_name");
                    Log.d("ryet", operator_name + " ");
                    txtServiceProvider.setText(operator_name + "");
                    //new CheckBBPSStatus().execute();
                    switch (operator_name) {
                        case "ACT Fibernet":
                            textView19.setText("Account Number OR User Name");
                            edtCustomerId.setHint("Enter account no. or user name");
                            new CheckStatus().execute();
                            break;
                        case "Connect Broadband":
                            textView19.setText("Directory Number");
                            edtCustomerId.setHint("Enter directory no");
                            new CheckStatus().execute();
                            break;
                        case "Hathway Broadband":
                            textView19.setText("Customer ID");
                            edtCustomerId.setHint("Enter customer id");
                            new CheckStatus().execute();
                            break;
                        case "Tikona Broadband":
                            textView19.setText("Service ID");
                            edtCustomerId.setHint("Enter service id");
                            new CheckStatus().execute();
                            break;
                    }
                }
                break;
        }
    }

    @Override    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0

                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    gps = new GPSTracker(mContext, getActivity());

                    // Check if GPS enabled

                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();

                       /* Toast.makeText(getActivity().getApplicationContext(),
                                "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();*/
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(mContext, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
