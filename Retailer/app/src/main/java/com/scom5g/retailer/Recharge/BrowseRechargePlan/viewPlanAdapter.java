package com.scom5g.retailer.Recharge.BrowseRechargePlan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;

import java.util.List;

public class viewPlanAdapter extends RecyclerView.Adapter<viewPlanAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_validity,txt_amount,txt_description,txt_last_update;

        public MyViewHolder(View view) {
            super(view);
            txt_validity = (TextView) view.findViewById(R.id.txt_validity);
            txt_amount = (TextView) view.findViewById(R.id.txt_amount);
            txt_description = (TextView) view.findViewById(R.id.txt_description);
            txt_last_update = (TextView) view.findViewById(R.id.txt_last_update);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String amount=txt_amount.getText().toString();
                    Intent intent=new Intent(ctx, FulTTPlanFragment.class);
                    intent.putExtra("amount",amount);
                    ((Activity) ctx).setResult(21,intent);
                    ((Activity) ctx).finish();//finishing activity
                }
            });
        }
    }

    public viewPlanAdapter(List<Item> moviesList, Context ctx) {
        this.moviesList = moviesList;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_view_plan, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Item movie = moviesList.get(position);
        holder.txt_validity.setText(movie.getLabel1());
        holder.txt_amount.setText(movie.getLabel2());
        holder.txt_description.setText(movie.getLabel3());
        holder.txt_last_update.setText(movie.getDattime());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
