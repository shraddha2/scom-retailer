package com.scom5g.retailer.Recharge;

import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.scom5g.retailer.HomePage.HomeFragment;
import com.scom5g.retailer.Model.Item;
import com.scom5g.retailer.R;

import java.util.ArrayList;
import java.util.List;

public class DTHOperatorFragment extends Fragment {

    GridView gridView;
    private List<Item> rp_data;
    String trans_type="";
    TextView txt_title,txt_sub_title;

    public DTHOperatorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView= inflater.inflate(R.layout.fragment_mobile_operator, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);
        //getActivity().setTitle("Recharges(Choose Operator)");
        Bundle bundle = getArguments();
        trans_type = bundle.getString("trans_type");
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        //txt_sub_title= (TextView) rootView.findViewById(R.id.txt_sub_title);
        txt_title.setText("DTH Recharge (Choose Operator)");
        rp_data = fill_with_data2();
        // Instance of ImageAdapter Class
        GridView gridView = (GridView) rootView.findViewById(R.id.gridview);
        GridRechargeAdapter2 booksAdapter = new GridRechargeAdapter2(rp_data, getActivity());
        gridView.setAdapter(booksAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String operator_name = rp_data.get(position).txt.toString();
                String operator_id = rp_data.get(position).txt_id.toString();
                Bundle bundle=new Bundle();
                bundle.putString("operator_name", operator_name);
                bundle.putString("operator_id", operator_id);
                android.app.Fragment frg = new DTHRechargeFragment();
                frg.setArguments(bundle);
                ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                        .replace(R.id.frg_replace, frg,"DTH")
                        .addToBackStack(null)
                        .commit();
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    public List<Item> fill_with_data2() {

        List<Item> data = new ArrayList<>();
        data.add(new Item( R.drawable.ic_airtel_tv, "Airtel Digital Tv","19"));
        data.add(new Item( R.drawable.ic_tata_sky, "Tata Sky","20"));
        data.add(new Item( R.drawable.ic_dish_tv, "Dish Tv","21"));
        data.add(new Item( R.drawable.ic_videocon, "Videocon d2h","22"));
        data.add(new Item( R.drawable.ic_big_tv, "Big TV","23"));
        data.add(new Item( R.drawable.ic_sun_direct, "Sun Direct","24"));

        return data;
    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }
                return false;
            }
        });
    }

}
